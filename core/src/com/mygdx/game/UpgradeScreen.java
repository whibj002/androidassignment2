package com.mygdx.game;

import java.util.ArrayList;
import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Background.ScrollingBackground;

public class UpgradeScreen implements Screen {

    private MyGdxGame game;
    private SaveFile saveFile;

    private float deltaTime;

    private float h;
    private float w;

    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;
    private Stage stage;

    //Sound
    private Sound buttonPressSFX;

    //Skin
    private Skin skin;

    //Title
    private Texture titleTexture;
    private Image titleImage;

    //BG
    private ScrollingBackground background;

    //Back button
    private TextButton back;

    //VC label/icon
    private Label vcAmount;
    private TextureRegion vcTexture;

    //Bars
    private Texture bar;
    private float healthBar;
    private float damageBar;
    private float defenseBar;
    private float shotSpeedBar;
    private float fireRateBar;
    private float projectilesBar;

    //TextButtons
    private TextButton buyHealth;
    private TextButton buyDamage;
    private TextButton buyDefense;
    private TextButton buyShotSpeed;
    private TextButton buyFireRate;
    private TextButton buyProjectile;

    //Button colours
    private final Color red = new Color(Color.RED);
    private final Color green = new Color(Color.GREEN);
    private final Color blue = new Color(Color.BLUE);

    //Level Info Labels
    private Label healthLabel;
    private Label damageLabel;
    private Label defenseLabel;
    private Label shotSpeedLabel;
    private Label fireRateLabel;
    private Label projectileLabel;

    //VC required Labels
    private Label healthVCLabel;
    private Label damageVCLabel;
    private Label defenseVCLabel;
    private Label shotSpeedVCLabel;
    private Label fireRateVCLabel;
    private Label projectileVCLabel;

    //ButtonList
    private ArrayList<TextButton> buttons;

    //VC required per level arrays
    private int[] healthLevels;
    private int[] damageLevels;
    private int[] defenseLevels;
    private int[] shotSpeedLevels;
    private int[] fireRateLevels;
    private int[] projectileLevels;

    //Button cooldown
    private float cooldown = 0f;

    public UpgradeScreen(MyGdxGame game) {
        this.game = game;
        this.saveFile = new SaveFile();
    }

    public void create() {
        h = Gdx.graphics.getHeight();
        w = Gdx.graphics.getWidth();

        saveFile = new SaveFile();

        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        stage = new Stage();

        //Sound
        buttonPressSFX = Gdx.audio.newSound((Gdx.files.internal("menuSfx/buttonPress.wav")));

        //Skin
        skin = new Skin(Gdx.files.internal("gui/uiskin.json"));

        // Setup title logo
        titleTexture = new Texture(Gdx.files.internal("upgrades.png"));
        titleImage = new Image(titleTexture);
        stage.addActor(titleImage);
        titleImage.setPosition(0, h / 1.4f);
        titleImage.setZIndex(1);
        titleImage.setSize(w, h / 5);

        // Setup scrolling background
        background = new ScrollingBackground("spaceBackground2.png", 500);

        //Map VC costs to levels.
        //Health
        int baseCost = 500;
        healthLevels = new int[saveFile.getHealthCap() + 2];
        healthLevels[0] = 0;

        for (int i = 1; i < healthLevels.length; i++) {
            if (i == healthLevels.length - 1) {
                healthLevels[i] = 0;
            } else {
                healthLevels[i] = baseCost;
                baseCost *= 1.1;
            }
        }
        //Damage
        baseCost = 500;
        damageLevels = new int[saveFile.getDamageCap() + 2];
        damageLevels[0] = 0;

        for (int i = 1; i < damageLevels.length; i++) {
            if (i == damageLevels.length - 1) {
                damageLevels[i] = 0;
            } else {
                damageLevels[i] = baseCost;
                baseCost *= 1.1;
            }
        }
        //Defense
        baseCost = 500;
        defenseLevels = new int[saveFile.getDefenseCap() + 2];
        defenseLevels[0] = 0;

        for (int i = 1; i < defenseLevels.length; i++) {
            if (i == defenseLevels.length - 1) {
                damageLevels[i] = 0;
            }
            defenseLevels[i] = baseCost;
            baseCost *= 1.1;
        }

        //ShotSpeed/FireRate/Projectiles
        shotSpeedLevels = new int[]{0, 2500, 5000, 10000, 25000, 50000, 100000};
        fireRateLevels = new int[]{0, 2500, 5000, 10000, 25000, 50000, 100000};
        projectileLevels = new int[]{0, 25000, 50000, 100000, 250000, 500000, 1000000};

        //Purchase buttons
        buyHealth = new TextButton("+", skin);
        buyDamage = new TextButton("+", skin);
        buyDefense = new TextButton("+", skin);
        buyShotSpeed = new TextButton("+", skin);
        buyFireRate = new TextButton("+", skin);
        buyProjectile = new TextButton("+", skin);

        //Button size
        buyHealth.setSize(w / 10, h / 20);
        buyDamage.setSize(w / 10, h / 20);
        buyDefense.setSize(w / 10, h / 20);
        buyShotSpeed.setSize(w / 10, h / 20);
        buyFireRate.setSize(w / 10, h / 20);
        buyProjectile.setSize(w / 10, h / 20);

        //Button Position
        buyHealth.setPosition(50, h / 1.3f - 125);
        buyDamage.setPosition(50, h / 1.3f - 200);
        buyDefense.setPosition(50, h / 1.3f - 275);
        buyShotSpeed.setPosition(50, h / 1.3f - 350);
        buyFireRate.setPosition(50, h / 1.3f - 425);
        buyProjectile.setPosition(50, h / 1.3f - 500);

        //Button Colour
        buyHealth.setColor(green);
        buyDamage.setColor(green);
        buyDefense.setColor(green);
        buyShotSpeed.setColor(green);
        buyFireRate.setColor(green);
        buyProjectile.setColor(green);

        //Button list
        buttons = new ArrayList<>();
        buttons.addAll(Arrays.asList(buyHealth, buyDamage, buyDefense, buyShotSpeed, buyFireRate, buyProjectile));

        //Add to stage
        for (int i = 0; i < buttons.size(); i++) {
            stage.addActor(buttons.get(i));
        }

        //Level Labels
        healthLabel = new Label("Health: Level " + (saveFile.getHealthLevel()) + "/50", skin);
        damageLabel = new Label("Damage: Level " + (saveFile.getDamageLevel()) + "/50", skin);
        defenseLabel = new Label("Defense: Level " + (saveFile.getDefenseLevel() + "/50"), skin);
        shotSpeedLabel = new Label("Shot Speed: Level " + (saveFile.getShotSpeedLevel() + "/5"), skin);
        fireRateLabel = new Label("Fire Rate: Level " + (saveFile.getFireRateLevel() + "/5"), skin);
        projectileLabel = new Label("Projectiles: Level " + (saveFile.getProjectileLevel() + "/5"), skin);

        //Level Label Positioning
        healthLabel.setPosition(120, buyHealth.getY() + 10);
        damageLabel.setPosition(120, buyDamage.getY() + 10);
        defenseLabel.setPosition(120, buyDefense.getY() + 10);
        shotSpeedLabel.setPosition(120, buyShotSpeed.getY() + 10);
        fireRateLabel.setPosition(120, buyFireRate.getY() + 10);
        projectileLabel.setPosition(120, buyProjectile.getY() + 10);

        //Add to stage
        stage.addActor(healthLabel);
        stage.addActor(damageLabel);
        stage.addActor(defenseLabel);
        stage.addActor(shotSpeedLabel);
        stage.addActor(fireRateLabel);
        stage.addActor(projectileLabel);

        //VC Required Labels
        healthVCLabel = new Label(Integer.toString(healthLevels[saveFile.getHealthLevel() + 1]), skin);
        damageVCLabel = new Label(Integer.toString(damageLevels[saveFile.getDamageLevel() + 1]), skin);
        defenseVCLabel = new Label(Integer.toString(defenseLevels[saveFile.getDefenseLevel() + 1]), skin);
        shotSpeedVCLabel = new Label(Integer.toString(shotSpeedLevels[saveFile.getShotSpeedLevel() + 1]), skin);
        fireRateVCLabel = new Label(Integer.toString(fireRateLevels[saveFile.getFireRateLevel() + 1]), skin);
        projectileVCLabel = new Label(Integer.toString(projectileLevels[saveFile.getProjectileLevel() + 1]), skin);

        //VC Required Labels Positioning
        healthVCLabel.setPosition(w / 2.5f + 160, h / 1.3f - 118);
        damageVCLabel.setPosition(w / 2.5f + 160, h / 1.3f - 193);
        defenseVCLabel.setPosition(w / 2.5f + 160, h / 1.3f - 268);
        shotSpeedVCLabel.setPosition(w / 2.5f + 160, h / 1.3f - 343);
        fireRateVCLabel.setPosition(w / 2.5f + 160, h / 1.3f - 418);
        projectileVCLabel.setPosition(w / 2.5f + 160, h / 1.3f - 493);

        //Add to stage
        stage.addActor(healthVCLabel);
        stage.addActor(damageVCLabel);
        stage.addActor(defenseVCLabel);
        stage.addActor(shotSpeedVCLabel);
        stage.addActor(fireRateVCLabel);
        stage.addActor(projectileVCLabel);

        // Back to menu button
        back = new TextButton("Back", skin);
        back.pad(10);
        back.getLabel().setFontScale(1.5f, 1.5f);
        back.setSize(w / 6f, h / 16);
        back.setPosition(0, h - back.getHeight());

        // Virtual currency amount + icon
        vcTexture = new TextureRegion(new Texture("vc.png"));
        vcAmount = new Label(Integer.toString(saveFile.getVC()), skin);
        vcAmount.setFontScale(2f);
        vcAmount.setPosition(65, 30);

        //Add to stage
        stage.addActor(back);
        stage.addActor(vcAmount);

        //Bars
        bar = new Texture("blank.png");

        Gdx.input.setInputProcessor(stage);
        initClickListeners();
    }

    public void update() {
        deltaTime = Gdx.graphics.getDeltaTime();
        background.update(deltaTime);
        cooldown -= deltaTime;

        // Update virtual currency amount
        vcAmount.setText(saveFile.getVC());

        //Update the width multiplier for bars.
        healthBar = (float) saveFile.getHealthLevel() / saveFile.getHealthCap();
        damageBar = (float) saveFile.getDamageLevel() / saveFile.getDamageCap();
        defenseBar = (float) saveFile.getDefenseLevel() / saveFile.getDefenseCap();
        shotSpeedBar = (float) saveFile.getShotSpeedLevel() / saveFile.getShotSpeedCap();
        fireRateBar = (float) saveFile.getFireRateLevel() / saveFile.getFireRateCap();
        projectilesBar = (float) saveFile.getProjectileLevel() / saveFile.getProjectileCap();

        if (saveFile.getHealthLevel() == saveFile.getHealthCap()) {
            buyHealth.setTouchable(Touchable.disabled);
            buyHealth.setColor(blue);
        } else if (saveFile.getVC() < healthLevels[saveFile.getHealthLevel() + 1]) {
            buyHealth.setTouchable(Touchable.disabled);
            buyHealth.setColor(red);
        }
        if (saveFile.getDamageLevel() == saveFile.getDamageCap()) {
            buyDamage.setTouchable(Touchable.disabled);
            buyDamage.setColor(blue);
        } else if (saveFile.getVC() < damageLevels[saveFile.getDamageLevel() + 1]) {
            buyDamage.setTouchable(Touchable.disabled);
            buyDamage.setColor(red);
        }
        if (saveFile.getDefenseLevel() == saveFile.getDefenseCap()) {
            buyDefense.setTouchable(Touchable.disabled);
            buyDefense.setColor(blue);
        } else if (saveFile.getVC() < defenseLevels[saveFile.getDefenseLevel() + 1]) {
            buyDefense.setTouchable(Touchable.disabled);
            buyDefense.setColor(red);
        }
        if (saveFile.getShotSpeedLevel() == saveFile.getShotSpeedCap()) {
            buyShotSpeed.setTouchable(Touchable.disabled);
            buyShotSpeed.setColor(blue);
        } else if (saveFile.getVC() < shotSpeedLevels[saveFile.getShotSpeedLevel() + 1]) {
            buyShotSpeed.setTouchable(Touchable.disabled);
            buyShotSpeed.setColor(red);
        }
        if (saveFile.getFireRateLevel() == saveFile.getFireRateCap()) {
            buyFireRate.setTouchable(Touchable.disabled);
            buyFireRate.setColor(blue);
        } else if (saveFile.getVC() < fireRateLevels[saveFile.getFireRateLevel() + 1]) {
            buyFireRate.setTouchable(Touchable.disabled);
            buyFireRate.setColor(red);
        }
        if (saveFile.getProjectileLevel() == saveFile.getProjectileCap()) {
            buyProjectile.setTouchable(Touchable.disabled);
            buyProjectile.setColor(blue);
        } else if (saveFile.getVC() < projectileLevels[saveFile.getProjectileLevel() + 1]) {
            buyProjectile.setTouchable(Touchable.disabled);
            buyProjectile.setColor(red);
        }
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update();

        batch.begin();
        batch.setColor(Color.WHITE);
        background.render(batch);
        batch.draw(vcTexture, 0, 0);
        batch.draw(vcTexture, w / 2.5f + 120, h / 1.3f - 125, w / 12, h / 22);
        batch.draw(vcTexture, w / 2.5f + 120, h / 1.3f - 200, w / 12, h / 22);
        batch.draw(vcTexture, w / 2.5f + 120, h / 1.3f - 275, w / 12, h / 22);
        batch.draw(vcTexture, w / 2.5f + 120, h / 1.3f - 350, w / 12, h / 22);
        batch.draw(vcTexture, w / 2.5f + 120, h / 1.3f - 425, w / 12, h / 22);
        batch.draw(vcTexture, w / 2.5f + 120, h / 1.3f - 500, w / 12, h / 22);
        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.DARK_GRAY);
        shapeRenderer.rect(110, h / 1.3f - 123, w / 2.5f, h / 22);
        shapeRenderer.rect(110, h / 1.3f - 198, w / 2.5f, h / 22);
        shapeRenderer.rect(110, h / 1.3f - 273, w / 2.5f, h / 22);
        shapeRenderer.rect(110, h / 1.3f - 348, w / 2.5f, h / 22);
        shapeRenderer.rect(110, h / 1.3f - 423, w / 2.5f, h / 22);
        shapeRenderer.rect(110, h / 1.3f - 498, w / 2.5f, h / 22);
        shapeRenderer.end();

        batch.begin();
        batch.setColor(Color.GRAY);
        batch.draw(bar, 110, h / 1.3f - 123, w / 2.5f * healthBar, h / 22);
        batch.draw(bar, 110, h / 1.3f - 198, w / 2.5f * damageBar, h / 22);
        batch.draw(bar, 110, h / 1.3f - 273, w / 2.5f * defenseBar, h / 22);
        batch.draw(bar, 110, h / 1.3f - 348, w / 2.5f * shotSpeedBar, h / 22);
        batch.draw(bar, 110, h / 1.3f - 423, w / 2.5f * fireRateBar, h / 22);
        batch.draw(bar, 110, h / 1.3f - 498, w / 2.5f * projectilesBar, h / 22);
        batch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        titleTexture.dispose();
        buttonPressSFX.dispose();
        skin.dispose();
        shapeRenderer.dispose();
        batch.dispose();
        stage.dispose();
    }

    public void initClickListeners() {
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });

        buyHealth.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Prevents glitches from spam clicking.
                if (cooldown <= 0) {
                    //If the current level is less than the cap
                    if (saveFile.getHealthLevel() < saveFile.getHealthCap()) {
                        if (saveFile.getSound()) buttonPressSFX.play();
                        saveFile.decreaseVC(healthLevels[saveFile.getHealthLevel() + 1]);
                        saveFile.increaseHealth();
                        healthLabel.setText("Health: Level " + (saveFile.getHealthLevel()) + "/50");
                        cooldown = 0.2f;

                        //If the current level (after upgrading) is less than the cap.
                        if (saveFile.getHealthLevel() < saveFile.getHealthCap()) {
                            healthVCLabel.setText(Integer.toString(healthLevels[saveFile.getHealthLevel() + 1]));
                        } else {
                            buyHealth.setTouchable(Touchable.disabled);
                            buyHealth.setColor(blue);
                            healthVCLabel.setText(0);
                        }
                    }
                }
            }
        });

        buyDamage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (cooldown <= 0) {
                    if (saveFile.getDamageLevel() < saveFile.getDamageCap()) {
                        if (saveFile.getSound()) buttonPressSFX.play();
                        saveFile.decreaseVC(damageLevels[saveFile.getDamageLevel() + 1]);
                        saveFile.increaseDamage();
                        damageLabel.setText("Damage: Level " + (saveFile.getDamageLevel()) + "/50");
                        cooldown = 0.2f;

                        if (saveFile.getDamageLevel() < saveFile.getDamageCap()) {
                            damageVCLabel.setText(Integer.toString(damageLevels[saveFile.getDamageLevel() + 1]));
                        } else {
                            buyDamage.setTouchable(Touchable.disabled);
                            buyDamage.setColor(blue);
                            damageVCLabel.setText(0);
                        }
                    }
                }
            }
        });

        buyDefense.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (cooldown <= 0) {
                    if (saveFile.getDefenseLevel() < saveFile.getDefenseCap()) {
                        if (saveFile.getSound()) buttonPressSFX.play();
                        saveFile.decreaseVC(defenseLevels[saveFile.getDefenseLevel() + 1]);
                        saveFile.increaseDefense();
                        defenseLabel.setText("Defense: Level " + (saveFile.getDefenseLevel()) + "/50");
                        cooldown = 0.2f;

                        if (saveFile.getDefenseLevel() < saveFile.getDefenseCap()) {
                            defenseVCLabel.setText(Integer.toString(defenseLevels[saveFile.getDefenseLevel() + 1]));
                        } else {
                            buyDefense.setTouchable(Touchable.disabled);
                            buyDefense.setColor(blue);
                            defenseVCLabel.setText(0);
                        }
                    }
                }
            }
        });

        buyShotSpeed.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (cooldown <= 0) {
                    if (saveFile.getShotSpeedLevel() < saveFile.getShotSpeedCap()) {
                        if (saveFile.getSound()) buttonPressSFX.play();
                        saveFile.decreaseVC(shotSpeedLevels[saveFile.getShotSpeedLevel() + 1]);
                        saveFile.increaseShotSpeed();
                        shotSpeedLabel.setText("Shot Speed: Level " + (saveFile.getShotSpeedLevel()) + "/5");
                        cooldown = 0.2f;

                        if (saveFile.getShotSpeedLevel() < saveFile.getShotSpeedCap()) {
                            shotSpeedVCLabel.setText(Integer.toString(shotSpeedLevels[saveFile.getShotSpeedLevel() + 1]));
                        } else {
                            buyShotSpeed.setTouchable(Touchable.disabled);
                            buyShotSpeed.setColor(blue);
                            shotSpeedVCLabel.setText(0);
                        }
                    }
                }
            }
        });

        buyFireRate.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (cooldown <= 0) {
                    if (saveFile.getFireRateLevel() < saveFile.getFireRateCap()) {
                        if (saveFile.getSound()) buttonPressSFX.play();
                        saveFile.decreaseVC(fireRateLevels[saveFile.getFireRateLevel() + 1]);
                        saveFile.increaseFireRate();
                        fireRateLabel.setText("Fire Rate: Level " + (saveFile.getFireRateLevel()) + "/5");
                        cooldown = 0.2f;

                        if (saveFile.getFireRateLevel() < saveFile.getFireRateCap()) {
                            fireRateVCLabel.setText(Integer.toString(fireRateLevels[saveFile.getFireRateLevel() + 1]));
                        } else {
                            buyFireRate.setTouchable(Touchable.disabled);
                            buyFireRate.setColor(blue);
                            fireRateVCLabel.setText(0);
                        }
                    }
                }
            }
        });

        buyProjectile.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (cooldown <= 0) {
                    if (saveFile.getProjectileLevel() < saveFile.getProjectileCap()) {
                        if (saveFile.getSound()) buttonPressSFX.play();
                        saveFile.decreaseVC(projectileLevels[saveFile.getProjectileLevel() + 1]);
                        saveFile.increaseProjectiles();
                        projectileLabel.setText("Projectiles: Level " + (saveFile.getProjectileLevel()) + "/5");
                        cooldown = 0.2f;

                        if (saveFile.getProjectileLevel() < saveFile.getProjectileCap()) {
                            projectileVCLabel.setText(Integer.toString(projectileLevels[saveFile.getProjectileLevel() + 1]));
                        } else {
                            buyProjectile.setTouchable(Touchable.disabled);
                            buyProjectile.setColor(blue);
                            projectileVCLabel.setText(0);
                        }
                    }
                }
            }
        });
    }
}

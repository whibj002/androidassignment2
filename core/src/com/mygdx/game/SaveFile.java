package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class SaveFile {
    private Preferences prefs;

    //Player stats
    private int health;
    private int damage;
    private float defense;
    private float shotSpeed;
    private float fireRate;
    private int projectiles;
    private int vc;

    //Player upgrade levels
    private int healthLevel;
    private int damageLevel;
    private int defenseLevel;
    private int shotSpeedLevel;
    private int fireRateLevel;
    private int projectileLevel;

    //Player upgrade level capacities
    private int healthCap;
    private int damageCap;
    private int defenseCap;
    private int shotSpeedCap;
    private int fireRateCap;
    private int projectileCap;

    //Completed game levels.
    private int currentLevel;
    private int maxLevels;
    private int highestCompletedLevel;

    //Settings
    private boolean music;
    private boolean sound;

    public SaveFile() {
        prefs = Gdx.app.getPreferences("SaveFile");
        setValues();
    }

    private void setValues() {
        health = prefs.getInteger("health", 100);
        damage = prefs.getInteger("damage", 25);
        defense = prefs.getFloat("defense", 1f);
        shotSpeed = prefs.getFloat("shotSpeed", 500f);
        fireRate = prefs.getFloat("fireRate", 200f);
        projectiles = prefs.getInteger("projectiles", 1);

        healthLevel = prefs.getInteger("healthLevel", 0);
        damageLevel = prefs.getInteger("damageLevel", 0);
        defenseLevel = prefs.getInteger("defenseLevel", 0);
        shotSpeedLevel = prefs.getInteger("shotSpeedLevel", 0);
        fireRateLevel = prefs.getInteger("fireRateLevel", 0);
        projectileLevel = prefs.getInteger("projectileLevel", 0);

        healthCap = prefs.getInteger("healthCap", 50);
        damageCap = prefs.getInteger("damageCap", 50);
        defenseCap = prefs.getInteger("defenseCap", 50);
        shotSpeedCap = prefs.getInteger("shotSpeedCap", 5);
        fireRateCap = prefs.getInteger("fireRateCap", 5);
        projectileCap = prefs.getInteger("projectileCap", 5);

        vc = prefs.getInteger("vc", 0);

        currentLevel = prefs.getInteger("currentLevel", 0);
        maxLevels = prefs.getInteger("maxLevels", 9);
        highestCompletedLevel = prefs.getInteger("highestCompletedLevel", 0);

        music = prefs.getBoolean("music", true);
        sound = prefs.getBoolean("sound", true);
    }

    //Player Attributes
    public int getHealth() {
        return health;
    }

    public int getDamage() {
        return damage;
    }

    public float getDefense() {
        return defense;
    }

    public float getShotSpeed() {
        return shotSpeed;
    }

    public float getFireRate() {
        return fireRate;
    }

    public int getProjectiles() {
        return projectiles;
    }


    //Player Levels
    public int getHealthLevel() {
        return healthLevel;
    }

    public int getDamageLevel() {
        return damageLevel;
    }

    public int getDefenseLevel() {
        return defenseLevel;
    }

    public int getShotSpeedLevel() {
        return shotSpeedLevel;
    }

    public int getFireRateLevel() {
        return fireRateLevel;
    }

    public int getProjectileLevel() {
        return projectileLevel;
    }


    //Player Level caps
    public int getHealthCap() {
        return healthCap;
    }

    public int getDamageCap() {
        return damageCap;
    }

    public int getDefenseCap() {
        return defenseCap;
    }

    public int getShotSpeedCap() {
        return shotSpeedCap;
    }

    public int getFireRateCap() {
        return fireRateCap;
    }

    public int getProjectileCap() {
        return projectileCap;
    }


    //Virtual Currency
    public int getVC() {
        return vc;
    }

    //Game Levels
    public int getCurrentLevel() {
        return currentLevel;
    }

    public int getMaxLevels() {
        return maxLevels;
    }

    public int getHighestCompletedLevel() {
        return highestCompletedLevel;
    }

    //Misc settings
    public boolean getMusic() {
        return music;
    }

    public boolean getSound() {
        return sound;
    }

    public void increaseHealth() {
        healthLevel++;
        health += 2;
        prefs.putInteger("healthLevel", healthLevel);
        prefs.putInteger("health", health);
        prefs.flush();
    }

    public void increaseDamage() {
        damageLevel++;
        damage += 2;
        prefs.putInteger("damageLevel", damageLevel);
        prefs.putInteger("damage", damage);
        prefs.flush();
    }

    public void increaseDefense() {
        defenseLevel++;
        defense += 0.1f;
        prefs.putInteger("defenseLevel", defenseLevel);
        prefs.putFloat("defense", defense);
        prefs.flush();
    }

    public void increaseShotSpeed() {
        shotSpeedLevel++;
        shotSpeed += 100f;
        prefs.putInteger("shotSpeedLevel", shotSpeedLevel);
        prefs.putFloat("shotSpeed", shotSpeed);
        prefs.flush();
    }

    public void increaseFireRate() {
        fireRateLevel++;
        fireRate -= 20f;
        prefs.putInteger("fireRateLevel", fireRateLevel);
        prefs.putFloat("fireRate", fireRate);
        prefs.flush();
    }

    public void increaseProjectiles() {
        projectileLevel++;
        projectiles++;
        prefs.putInteger("projectileLevel", projectileLevel);
        prefs.putInteger("projectiles", projectiles);
        prefs.flush();
    }

    public void increaseVC(int amount) {
        vc += amount;
        prefs.putInteger("vc", vc);
        prefs.flush();
    }

    public void decreaseVC(int amount) {
        vc -= amount;
        prefs.putInteger("vc", vc);
        prefs.flush();
    }

    public void increaseCurrentLevel() {
        currentLevel++;
        prefs.putInteger("currentLevel", currentLevel);
        prefs.flush();
    }

    public void setCurrentLevel(int currentLevelIndex) {
        currentLevel = currentLevelIndex;
        prefs.putInteger("currentLevel", currentLevelIndex);
        prefs.flush();
    }

    public void setHighestCompletedLevel(int currentLevelIndex) {
        if (currentLevelIndex > maxLevels) currentLevelIndex = maxLevels;

        highestCompletedLevel = currentLevelIndex;
        prefs.putInteger("highestCompletedLevel", highestCompletedLevel);
        prefs.flush();
    }

    public void musicOn() {
        music = true;
        prefs.putBoolean("music", true);
        prefs.flush();
    }

    public void musicOff() {
        music = false;
        prefs.putBoolean("music", false);
        prefs.flush();
    }

    public void soundOn() {
        sound = true;
        prefs.putBoolean("sound", true);
        prefs.flush();
    }

    public void soundOff() {
        sound = false;
        prefs.putBoolean("sound", false);
        prefs.flush();
    }

    protected void reset() {
        prefs.clear();
        setValues();
        prefs.flush();
    }

    protected void godMode() {
        boolean sound = this.sound;
        boolean music = this.music;
        prefs.clear();
        setValues();
        setHighestCompletedLevel(9);
        increaseVC(5000000);

        if (!sound) {
            prefs.putBoolean("sound", false);
        }
        if (!music) {
            prefs.putBoolean("music", false);
        }
        prefs.flush();
    }
}

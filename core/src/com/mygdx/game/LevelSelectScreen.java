package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Background.ScrollingBackground;

public class LevelSelectScreen implements Screen {

    // Instance variables
    MyGdxGame game;
    private SpriteBatch batch;
    private Stage stage;
    private float deltaTime;
    private ScrollingBackground background;
    private Skin skin;
    private Texture titleTexture;
    private TextureRegion titleTextureRegion;
    private Image titleImage;
    private Table table;
    private TextButton back;
    private Sound buttonPressSFX;
    private TextButton level1Button, level2Button, level3Button, level4Button, level5Button, level6Button, level7Button, level8Button, level9Button, level10Button;
    private SaveFile saveFile;

    public LevelSelectScreen(MyGdxGame game) {
        this.game = game;
        saveFile = new SaveFile();
    }

    /**
     * Initializing method that sets up the screen and it's contents.
     */
    public void create() {
        // Set up the batch and stage for drawing graphics, and initialize skin and background graphic.
        batch = new SpriteBatch();
        stage = new Stage();
        skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
        background = new ScrollingBackground("spaceBackground3.png", 250);
        buttonPressSFX = Gdx.audio.newSound((Gdx.files.internal("menuSfx/buttonPress.wav")));
        // Variables for referencing the width and height of the screen.
        float h = Gdx.graphics.getHeight();
        float w = Gdx.graphics.getWidth();

        // Setup the graphic's image, size and position for the SELECT STAGE logo.
        titleTexture = new Texture(Gdx.files.internal("selectStage.png"));
        titleTextureRegion = new TextureRegion(titleTexture);
        titleImage = new Image(titleTexture);
        titleImage.setZIndex(0);
        titleImage.setPosition(0, Gdx.graphics.getHeight() / 1.5f);
        titleImage.setZIndex(1);
        titleImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 5);

        // Setup Back Button's text, skin, size and position.
        back = new TextButton("Back", skin);
        back.pad(10);
        back.getLabel().setFontScale(1.5f, 1.5f);
        back.setSize(w / 6f, h / 16);
        back.setPosition(0, h - back.getHeight());


        // Create and set up table for housing level select buttons.
        table = new Table(skin);
        table.setSize(w / 1.2f, h / 1.6f);
        table.setPosition(w / 12, h / 35f);
        table.getSkin().getFont("default-font").getData().setScale(1.5f, 1.5f);

        // Setup Level Buttons
        level1Button = new TextButton("1", skin);
        level2Button = new TextButton("2", skin);
        level3Button = new TextButton("3", skin);
        level4Button = new TextButton("4", skin);
        level5Button = new TextButton("5", skin);
        level6Button = new TextButton("6", skin);
        level7Button = new TextButton("7", skin);
        level8Button = new TextButton("8", skin);
        level9Button = new TextButton("9", skin);
        level10Button = new TextButton("10", skin);

        // This code adds the buttons to the table and sets their positions and jazz
        table.add(level1Button).grow().pad(w / 40);
        table.add(level2Button).grow().pad(w / 40);
        table.add(level3Button).grow().pad(w / 40).row();
        table.add(level4Button).grow().pad(w / 40);
        table.add(level5Button).grow().pad(w / 40);
        table.add(level6Button).grow().pad(w / 40).row();
        table.add(level7Button).grow().pad(w / 40);
        table.add(level8Button).grow().pad(w / 40);
        table.add(level9Button).grow().pad(w / 40).row();
        table.add(level10Button).grow().pad(w / 40);
        table.add("").grow().pad(w / 40).row();

        // Lock levels the player doesn't have access to based on their progress stored in saveFile
        boolean locked = false;
        for (int i = 0; i <= saveFile.getMaxLevels() + 1; i++) {
            Array<Cell> buttons = table.getCells();

            if (i == saveFile.getHighestCompletedLevel() + 1) {
                locked = true;
            }
            if (locked) {
                buttons.get(i).getActor().setTouchable(Touchable.disabled);
                buttons.get(i).getActor().setColor(Color.GRAY);
            }
        }

        // Add components to stage
        stage.addActor(table);
        stage.addActor(titleImage);
        stage.addActor(back);

        // Add event listeners for each button
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                dispose();
                game.setScreen(new MenuScreen(game));

            }
        });

        // YOU CAN TURN THE BUTTONS ON AND OFF WITH THE setButtonEnabled(TextButton, Boolean)
        // method in this class
        // LISTENER FOR LEVEL 1 BUTTON
        level1Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(0);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 2 BUTTON
        level2Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(1);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 3 BUTTON
        level3Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(2);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 4 BUTTON
        level4Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(3);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 5 BUTTON
        level5Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playBossSong();
                saveFile.setCurrentLevel(4);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 6 BUTTON
        level6Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(5);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 7 BUTTON
        level7Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(6);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 8 BUTTON
        level8Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(7);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 9 BUTTON
        level9Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playGameplaySong();
                saveFile.setCurrentLevel(8);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));

            }
        });
        // LISTENER FOR LEVEL 10 BUTTON
        level10Button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getMusic()) game.songPlayer.playBossSong();
                saveFile.setCurrentLevel(9);
                if (saveFile.getSound()) buttonPressSFX.play();
                game.setScreen(new GameScreen(game));
            }
        });
        Gdx.input.setInputProcessor(stage);
    }


    public void update() {
        deltaTime = Gdx.graphics.getDeltaTime();
        background.update(deltaTime);
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update();

        batch.begin();
        background.render(batch);
        batch.end();

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        skin.dispose();
        buttonPressSFX.dispose();

    }


    /**
     * @param button The button to enable/disable
     * @param bool Whether the button should be enabled/disabled
     *             This method makes a button either normal, or greyed out and untouchable.
     *             Zac forgot it existed so it's redundant and isn't in use.
     */
    public void setButtonEnabled(TextButton button, boolean bool) {

        if (bool) {
            button.setTouchable(Touchable.enabled);
            button.setColor(1, 1, 1, 1);
        } else {
            button.setTouchable(Touchable.disabled);
            button.setColor(0.3f, 0.3f, 0.3f, 1);
        }
    }
}

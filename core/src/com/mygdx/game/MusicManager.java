package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MusicManager {

    public Music menuSong;
    public Music gameplaySong;
    public Music bossSong;

    public MusicManager() {
        //Using mp3s because wav files were trigger an insufficient storage error at random times.
        menuSong = Gdx.audio.newMusic(Gdx.files.internal("music/menuOST.mp3"));
        gameplaySong = Gdx.audio.newMusic(Gdx.files.internal("music/song3.mp3"));
        bossSong = Gdx.audio.newMusic(Gdx.files.internal("music/song2.mp3"));

        menuSong.setLooping(true);
        gameplaySong.setLooping(true);
        bossSong.setLooping(true);
    }

    public void playMenuSong() {
        menuSong.play();
        gameplaySong.stop();
        bossSong.stop();
    }

    public void playGameplaySong() {
        gameplaySong.play();
        menuSong.stop();
        bossSong.stop();
    }

    public void playBossSong() {
        bossSong.play();
        menuSong.stop();
        gameplaySong.stop();
    }

    public void stop() {
        menuSong.stop();
        gameplaySong.stop();
        bossSong.stop();
    }

    public void dispose() {
        menuSong.dispose();
        gameplaySong.dispose();
        bossSong.dispose();
    }
}

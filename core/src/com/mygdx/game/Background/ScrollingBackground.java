package com.mygdx.game.Background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ScrollingBackground {

    Texture image;
    float y1;
    float y2;
    int speed;

    public ScrollingBackground () {
        image = new Texture("spaceBackground1.png");
        y1 = 0;
        y2 = image.getHeight();
        speed = 25;
    }

    public ScrollingBackground (String fileName, int moveSpeed) {
        image = new Texture(fileName);
        y1 = 0;
        y2 = image.getHeight();
        speed = moveSpeed;
    }

    public void update (float deltaTime) {
        y1 -= speed * deltaTime;
        y2 -= speed * deltaTime;

        //if the image reaches the bottom of screen and is not visible, put it back on top
        if (y1 + image.getHeight() <= 0)
            y1 = y2 + image.getHeight();

        if (y2 + image.getHeight() <= 0)
            y2 = y1 + image.getHeight();
    }

    public void render (SpriteBatch batch) {
        batch.draw(image, -2500, y1, image.getWidth(), image.getHeight());
        batch.draw(image, -2500, y2, image.getWidth(), image.getHeight());
    }

    public void dispose() {
        image.dispose();
    }
}

package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Background.ScrollingBackground;
import java.util.ArrayList;

public class TutorialScreen implements Screen {

    // Instance variables
    MyGdxGame game;
    private SpriteBatch batch;
    private Stage stage;
    private float deltaTime;
    private ScrollingBackground background;
    private Skin skin;
    private Texture titleTexture;
    private TextureRegion titleTextureRegion;
    private Image titleImage;
    private TextButton back, howToPlay, powerUps, upgradeScreen, enemies;
    private Sound buttonPressSFX;
    private boolean showingPrimaryTutorialMenu;

    // How to Play The Game Tutorial components
    private Texture howToPlayTexture;
    private TextureRegion howToPlayTextureRegion;
    private Image howToPlayImage;
    private Label howToPlayText;
    // Powerup Tutorial components
    private Texture powerUpsTexture;
    private TextureRegion powerUpsTextureRegion;
    private Image powerUpsImage;
    private Label powerUpsText;
    // Upgrades Screen Tutorial components
    private Texture upgradeScreenTexture;
    private TextureRegion upgradeScreenTextureRegion;
    private Image upgradeScreenImage;
    private Label upgradeScreenText;
    // Enemies Screen Tutorial Components
    private Label enemySpriteText;
    // Long descriptions for enemy units, to be used in the enemy gallery
    private String[] enemyDescriptions = new String[]{"A standard Cockroach battlecruiser, commonly seen in swarms. Though individually not threatening, they can be overwhelming when they work together" +
            "with other star ships. It remains stationary while firing rounds of three bullets towards the player.",
            "A middle-ranked Wasp Lancer ship, these units indicate their line of fire with a white line before firing a powerful red beam. Though the laser takes a moment to fire and has it's " +
                    "aim given away, the laser deals very high damage. Prioritize moving out of their line of fire.",
            "A commander-class bomber. They are known for their high power scatter bombs, a slow moving projectile that detonates after a short period of time and scatters smaller bullets around it." +
                    "Though they have heavy firepower, these ships are assigned commanding roles and thus will hide behind other ships wherever possible, keeping their distance from their enemies.",
            "A high speed spider interceptor, these ships lack heavy firepower but excel in mobility. They will constantly weave about the battlefield, rapidly firing their dual shot cannons. Though" +
                    " their damage output is not overly dangerous, they can be difficult to pin down, and they will chip away at ships defences with constant bombardments.",
            "An ion-plasma beetle cruiser. Manned by only the most insane of pilots, these ships fire ion bursts that don't deal any damage, but will disable the weapons of any ship that they hit. " +
                    " While this alone is dangerous, the ship will attempt to kamikaze their victims once they disable their weapon, charging and crashing into them. This collision must be avoided" +
                    " at all costs, as a head on collision will almost always result in massive damage, if not death.",
            "A dreadnought class black hole launcher. These ships are relatively slow moving, but will periodically fire a neutron eradicator bomb. After a few moments, this bomb will detonate and " +
                    " create a black hole. This black hole will suck up both projectiles and ships, making it difficult to shoot at them. If the black hole begins to suck up your ship, move away" +
                    " immediately, as a black hole will instantly destroy your ship on contact.",
            "An elite-ranked Cockroach battlecruiser. Very similar to the standard variant, this battleship has been upgraded with enhanced armour and larger weapon stocks, allowing it to fire larger" +
                    " volleys of bullets and soak up more damage before going down. Exercise caution in engaging them, as their barrages are difficult to navigate around.",
            "An elite-ranked Wasp Lancer ship. These units are unusual, in that while they have upgraded armour compared to the standard counterpart, their crew will purposely aim near the player sometimes" +
                    " instead of right at them. Be careful that you don't accidentally move into their line of fire, expecting them to shoot right at you.",
            "An elite-ranked bomber, these devil-shaped ships will approach the player, unlike their standard versions. They have enhanced armour and mobility, and will constantly weave back and fourth" +
                    " just above the player, firing their bombs at a higher rate of fire. Eliminate them as quickly as possible to avoid having to deal with their barrage of scatter bombs.",};
    private ArrayList<Image> enemyImageArray = new ArrayList<Image>();
    private TextButton nextEnemy, previousEnemy;
    private int currentSpritePos = 0;

    private SaveFile saveFile;

    public TutorialScreen(MyGdxGame game) {
        this.game = game;
    }

    /**
     * Method for setting up the screen and it's contents.
     */
    public void create() {
        // Set up the batch and stage for drawing graphics, and initialize skin and background graphic.
        saveFile = new SaveFile();
        batch = new SpriteBatch();
        stage = new Stage();
        skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
        background = new ScrollingBackground("spaceBackground4.png", 75);
        buttonPressSFX = Gdx.audio.newSound((Gdx.files.internal("menuSfx/buttonPress.wav")));

        // Variables for referencing the width and height of the screen.
        float h = Gdx.graphics.getHeight();
        float w = Gdx.graphics.getWidth();

        // Setup Visuals
        // Setup title logo
        titleTexture = new Texture(Gdx.files.internal("selectTutorial.png"));
        titleTextureRegion = new TextureRegion(titleTexture);
        titleImage = new Image(titleTexture);
        titleImage.setPosition(0, Gdx.graphics.getHeight() / 1.5f);
        titleImage.setZIndex(1);
        titleImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 5f);

        // Setup How To Play Image
        howToPlayTexture = new Texture(Gdx.files.internal("howToPlayImage.png"));
        howToPlayTextureRegion = new TextureRegion(howToPlayTexture);
        howToPlayImage = new Image(howToPlayTexture);
        howToPlayImage.setPosition(w / 12f, h / 5f);
        howToPlayImage.setZIndex(1);
        howToPlayImage.setSize((w - (w / 6)), (h - (h / 3)));

        // Setup Powerups Image
        powerUpsTexture = new Texture(Gdx.files.internal("powerUpsImage.png"));
        powerUpsTextureRegion = new TextureRegion(powerUpsTexture);
        powerUpsImage = new Image(powerUpsTexture);
        powerUpsImage.setPosition(w / 12f, h / 5f);
        powerUpsImage.setZIndex(2);
        powerUpsImage.setSize((w - (w / 6)), (h - (h / 3)));

        // Setup Upgrade Screen Image
        upgradeScreenTexture = new Texture(Gdx.files.internal("upgradesImage.png"));
        upgradeScreenTextureRegion = new TextureRegion(upgradeScreenTexture);
        upgradeScreenImage = new Image(upgradeScreenTexture);
        upgradeScreenImage.setPosition(w / 12f, h / 5f);
        upgradeScreenImage.setZIndex(2);
        upgradeScreenImage.setSize((w - (w / 6)), (h - (h / 3)));

        // Add enemy images to array
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom3.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom2.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom1.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom4.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom5.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom6.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom3Elite.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom2Elite.png"))))));
        enemyImageArray.add(new Image(new TextureRegion(new Texture((Gdx.files.internal("enemyShipCustom1Elite.png"))))));

        // Set size and position for all images in the array
        for (Image i : enemyImageArray) {
            i.setPosition(w / 12f, h / 5f);
            i.setZIndex(2);
            i.setSize((w - (w / 6)), (h - (h / 3)));
            i.setVisible(false);
            stage.addActor(i);
        }

        // Setup text for tutorial screens

        // How To Play Tutorial
        howToPlayText = new Label("In order to maneuver your ship, you can touch your finger " +
                "to any position on the screen and drag. Your ship will follow your finger wherever " +
                "it goes. Your ship will automatically fire upwards. To reach the end of every " +
                "level, you must destroy all the enemy ships that come from above. Whenever your " +
                "ship is hit by a bullet, your green HP bar will decrease. If it reaches zero, it's " +
                "game over.", skin);
        howToPlayText.setWidth(w);
        howToPlayText.setHeight(h / 5);
        howToPlayText.setBounds(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f, w, h / 5);
        howToPlayText.setWrap(true);
        howToPlayText.setPosition(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f);

        // Powerups Tutorial
        powerUpsText = new Label("As you pilot your way through stages, various powerups will" +
                " drop after defeating waves of enemies. Powerups can increase the damage your ship" +
                " does, or decrease the amount of damage you take from enemy fire. They can also" +
                "increase or decrease your health, so pay attention. Finally, powerups only last" +
                "for a limited period of time, so keep an eye on the powerup gauges in the bottom" +
                "left of the screen.", skin);
        powerUpsText.setWidth(w);
        powerUpsText.setHeight(h / 5);
        powerUpsText.setBounds(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f, w, h / 5);
        powerUpsText.setWrap(true);
        powerUpsText.setPosition(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f);

        // Upgrade Screen Tutorial
        upgradeScreenText = new Label("As you play the game, you'll accumulate Space Coins. " +
                " With your Space Coins, you can visit the Upgrade Screen from the Main Menu. On" +
                " the Upgrade Screen, you can purchase permanent upgrades for your ship, such as " +
                "increased defences, bullet and weapon speed, and even the ability to fire an" +
                "increased amount of bullets.", skin);
        upgradeScreenText.setWidth(w);
        upgradeScreenText.setHeight(h / 5);
        upgradeScreenText.setBounds(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f, w, h / 5);
        upgradeScreenText.setWrap(true);
        upgradeScreenText.setPosition(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f);

        // Enemy Gallery text setup
        enemySpriteText = new Label(enemyDescriptions[0], skin);
        enemySpriteText.setWidth(w);
        enemySpriteText.setHeight(h / 5);
        enemySpriteText.setBounds(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f, w, h / 5);
        enemySpriteText.setWrap(true);
        enemySpriteText.setPosition(0, howToPlayImage.getY() - howToPlayText.getHeight() - 5f);


        // Setup Buttons
        // Back Button styling and position
        back = new TextButton("Back", skin);
        back.pad(10);
        back.getLabel().setFontScale(1.5f, 1.5f);
        back.setSize(w / 6f, h / 16);
        back.setPosition(0, h - back.getHeight());

        // Next enemy styling and position
        nextEnemy = new TextButton("Next Enemy", skin);
        nextEnemy.pad(10);
        nextEnemy.getLabel().setFontScale(1.5f, 1.5f);
        nextEnemy.setSize(w / 2.5f, h / 16);
        nextEnemy.setPosition(w - nextEnemy.getWidth() - 10f, (h - back.getHeight()) - back.getHeight() - 10f);

        // Previous enemy styling and position
        previousEnemy = new TextButton("Previous Enemy", skin);
        previousEnemy.pad(10);
        previousEnemy.getLabel().setFontScale(1.5f, 1.5f);
        previousEnemy.setSize(w / 2.5f, h / 16);
        previousEnemy.setPosition(10f, (h - back.getHeight()) - back.getHeight() - 10f);

        // Tutorial Buttons styling and position
        howToPlay = new TextButton("How to Play", skin);
        howToPlay.getLabel().setFontScale(1.5f, 1.5f);
        howToPlay.setSize(w / 1.2f, h / 10);
        howToPlay.setPosition(w / 12f, h / 2);
        powerUps = new TextButton("Power Ups", skin);
        powerUps.getLabel().setFontScale(1.5f, 1.5f);
        powerUps.setSize(w / 1.2f, h / 10);
        powerUps.setPosition(w / 12f, (howToPlay.getY() - (h / 10) - (h / 20)));
        upgradeScreen = new TextButton("The Upgrade Screen", skin);
        upgradeScreen.getLabel().setFontScale(1.5f, 1.5f);
        upgradeScreen.setSize(w / 1.2f, h / 10);
        upgradeScreen.setPosition(w / 12f, (powerUps.getY() - (h / 10) - (h / 20)));
        enemies = new TextButton("Enemies", skin);
        enemies.getLabel().setFontScale(1.5f, 1.5f);
        enemies.setSize(w / 1.2f, h / 10);
        enemies.setPosition(w / 12f, (upgradeScreen.getY() - (h / 10) - (h / 20)));

        // Add actors to stage
        stage.addActor(titleImage);
        stage.addActor(back);
        stage.addActor(howToPlay);
        stage.addActor(powerUps);
        stage.addActor(upgradeScreen);
        stage.addActor(enemies);
        stage.addActor(howToPlayImage);
        stage.addActor(powerUpsImage);
        stage.addActor(upgradeScreenImage);
        stage.addActor(howToPlayText);
        stage.addActor(powerUpsText);
        stage.addActor(upgradeScreenText);
        stage.addActor(enemySpriteText);
        stage.addActor(nextEnemy);
        stage.addActor(previousEnemy);

        // Add event listeners for each button

        // Back button. Functions differently based on if the main tutorial menu is showing, where it returns
        // to the main menu, or if a tutorial is showing, returns the visibility of components to the
        // main tutorial menu
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                if (showingPrimaryTutorialMenu) {

                    dispose();
                    game.setScreen(new MenuScreen(game));
                } else {
                    showingPrimaryTutorialMenu = true;
                    showTutorialButtons();
                    hideHowToPlay();
                    hideEnemies();
                    hidePowerUps();
                    hideUpgradeScreen();
                    currentSpritePos = 0;
                }
            }
        });

        // Listener that shows the how to play components and hides the main tutorial menu
        howToPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                showHowToPlay();
                showingPrimaryTutorialMenu = false;
            }
        });

        // Listener that shows the powerup tutorial components and hides the main tutorial menu
        powerUps.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                showPowerUps();
                showingPrimaryTutorialMenu = false;
            }
        });

        // Listener that shows the upgrade screen tutorial components and hides the main tutorial menu
        upgradeScreen.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                showUpgradeScreen();
                showingPrimaryTutorialMenu = false;
            }
        });

        // Listener that shows the enemy gallery components and hides the main tutorial menu
        enemies.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                showEnemies();
                showingPrimaryTutorialMenu = false;
            }
        });

        // Listener that pans to the next enemy in the gallery
        nextEnemy.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();

                if (currentSpritePos >= 0) {
                    currentSpritePos++;
                } else if (currentSpritePos < 0) {
                    currentSpritePos = 8;
                }

                if (currentSpritePos > 8) {
                    currentSpritePos = 0;
                }

                for (int i = 0; i < enemyImageArray.size(); i++) {
                    enemyImageArray.get(i).setVisible(false);
                    if (i == currentSpritePos) {
                        enemyImageArray.get(i).setVisible(true);
                    }
                }

                enemySpriteText.setText(enemyDescriptions[currentSpritePos]);
                System.out.println(currentSpritePos);
            }
        });

        // Listener that pans to the previous enemy in the gallery
        previousEnemy.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                if (currentSpritePos >= 0) {
                    currentSpritePos--;
                }

                if (currentSpritePos < 0) {
                    currentSpritePos = 8;
                }

                if (currentSpritePos > 8) {
                    currentSpritePos = 0;
                }

                for (int i = 0; i < enemyImageArray.size(); i++) {
                    enemyImageArray.get(i).setVisible(false);
                    if (i == currentSpritePos) {
                        enemyImageArray.get(i).setVisible(true);
                    }
                }

                enemySpriteText.setText(enemyDescriptions[currentSpritePos]);
                System.out.println(currentSpritePos);
            }
        });


        // Set initially visible components
        showTutorialButtons();
        showingPrimaryTutorialMenu = true;

        Gdx.input.setInputProcessor(stage);
    }


    public void update() {
        deltaTime = Gdx.graphics.getDeltaTime();
        background.update(deltaTime);
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update();

        batch.begin();
        background.render(batch);
        batch.end();

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        skin.dispose();
        buttonPressSFX.dispose();
        upgradeScreenTexture.dispose();
        howToPlayTexture.dispose();
        powerUpsTexture.dispose();
        titleTexture.dispose();


    }

    // Below are methods for showing and hiding the visibility of components

    public void showHowToPlay() {
        // Hide main tutorial buttons
        hideTutorialButtons();
        // Show components relevant to How To Play tutorial
        howToPlayImage.setVisible(true);
        howToPlayText.setVisible(true);

    }

    public void hideHowToPlay() {
        // Hide components relevant to How To Play tutorial
        howToPlayImage.setVisible(false);
        howToPlayText.setVisible(false);
    }

    public void showPowerUps() {
        // Hide main tutorial buttons
        hideTutorialButtons();
        // Show components relevant to powerUp tutorial
        powerUpsImage.setVisible(true);
        powerUpsText.setVisible(true);
    }

    public void hidePowerUps() {
        // Hide components relevant to powerUp tutorial
        powerUpsImage.setVisible(false);
        powerUpsText.setVisible(false);
    }

    public void showUpgradeScreen() {
        // Hide main tutorial buttons
        hideTutorialButtons();
        // Show components relevant to Upgrade tutorial
        upgradeScreenImage.setVisible(true);
        upgradeScreenText.setVisible(true);
    }

    public void hideUpgradeScreen() {
        // Hide components relevant to upgradeScreen tutorial
        upgradeScreenImage.setVisible(false);
        upgradeScreenText.setVisible(false);
    }

    public void showEnemies() {
        // Hide main tutorial buttons
        hideTutorialButtons();
        // Show components relevant to enemy tutorial
        //enemySpriteImage.setVisible(true);
        enemySpriteText.setVisible(true);
        enemyImageArray.get(currentSpritePos).setVisible(true);
        nextEnemy.setVisible(true);
        previousEnemy.setVisible(true);
    }

    public void hideEnemies() {
        //enemySpriteImage.setVisible(false);
        enemySpriteText.setVisible(false);
        nextEnemy.setVisible(false);
        previousEnemy.setVisible(false);
        for (Image i : enemyImageArray) {
            i.setVisible(false);
        }
    }

    public void showTutorialButtons() {
        // Set title image, back button and main tutorial buttons visible
        titleImage.setVisible(true);
        back.setVisible(true);
        howToPlay.setVisible(true);
        powerUps.setVisible(true);
        upgradeScreen.setVisible(true);
        enemies.setVisible(true);
        // Hide components of each tutorial section
        hideHowToPlay();
        hidePowerUps();
        hideUpgradeScreen();
        hideEnemies();
    }

    public void hideTutorialButtons() {
        // Set title image, back button and main tutorial buttons invisible
        titleImage.setVisible(false);
        howToPlay.setVisible(false);
        powerUps.setVisible(false);
        upgradeScreen.setVisible(false);
        enemies.setVisible(false);
    }
}

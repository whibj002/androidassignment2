package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Background.ScrollingBackground;

public class MenuScreen implements Screen {
    // Instance variables
    MyGdxGame game;
    private SpriteBatch batch;
    private Stage stage;
    private Texture titleTexture;
    private TextureRegion titleTextureRegion;
    private Image titleImage;
    private float deltaTime;
    // 1 = Main Menu, 2 = Level Select, 3 = Settings Menu
    private int currentMenu = 1;
    private Sound buttonPressSFX;
    // Background variable
    private ScrollingBackground background;

    // Instantiate these buttons and skin here so they can be accessed by functions immediately
    private Skin skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
    private TextButton playButton = new TextButton("Play", skin);
    private TextButton upgradeButton = new TextButton("Upgrades", skin);
    private TextButton tutorialButton = new TextButton("Tutorial", skin);
    private TextButton settingsButton = new TextButton("Settings", skin);
    private TextButton exitButton = new TextButton("Exit", skin);
    private TextButton backButton = new TextButton("Back", skin);

    // Graphics for the settings menu
    private Texture musicTexture;
    private TextureRegion musicTextureRegion;
    private Image musicImage;
    private Texture soundTexture;
    private TextureRegion soundTextureRegion;
    private Image soundImage;
    private Texture godTexture;
    private TextureRegion godTextureRegion;
    private Image godImage;
    private Texture resetTexture;
    private TextureRegion resetTextureRegion;
    private Image resetImage;
    // Table and buttons for the settings menu
    private Table settingsTable = new Table();
    private TextButton musicSettingButton;
    private TextButton soundSettingButton;
    private TextButton godModeSettingButton = new TextButton("GOD MODE", skin);
    private TextButton resetDataSettingButton = new TextButton("RESET DATA", skin);

    private SaveFile saveFile;

    // Constructor
    public MenuScreen(MyGdxGame game) {
        this.game = game;
    }

    public void create() {
        // Instantiate the saveFile and set up the batch and stage for drawing visuals
        saveFile = new SaveFile();
        batch = new SpriteBatch();
        stage = new Stage();

        // Instantiate variables used for getting the size of the screen
        float h = Gdx.graphics.getHeight();
        float w = Gdx.graphics.getWidth();

        // Setup title logo
        titleTexture = new Texture(Gdx.files.internal("spaceShooterLogo.png"));
        titleTextureRegion = new TextureRegion(titleTexture);
        titleImage = new Image(titleTexture);
        stage.addActor(titleImage);
        titleImage.setPosition(0, h / 1.5f);
        titleImage.setZIndex(1);
        titleImage.setSize(w, h / 5);

        // Setup images for settings menu
        musicTexture = new Texture(Gdx.files.internal("settingsMusic.png"));
        musicTextureRegion = new TextureRegion(musicTexture);
        musicImage = new Image(musicTexture);
        musicImage.setZIndex(1);
        soundTexture = new Texture(Gdx.files.internal("settingsSound.png"));
        soundTextureRegion = new TextureRegion(soundTexture);
        soundImage = new Image(soundTexture);
        soundImage.setZIndex(1);
        godTexture = new Texture(Gdx.files.internal("settingsGod.png"));
        godTextureRegion = new TextureRegion(godTexture);
        godImage = new Image(godTexture);
        godImage.setZIndex(1);
        resetTexture = new Texture(Gdx.files.internal("settingsReset.png"));
        resetTextureRegion = new TextureRegion(resetTexture);
        resetImage = new Image(resetTexture);
        resetImage.setZIndex(1);

        // Set the text of the buttons in settings based on preferences stored in saveFile
        if (saveFile.getMusic()) {
            musicSettingButton = new TextButton("ON", skin);
        } else {
            musicSettingButton = new TextButton("OFF", skin);
        }
        if (saveFile.getSound()) {
            soundSettingButton = new TextButton("ON", skin);
        } else {
            soundSettingButton = new TextButton("OFF", skin);
        }


        // Setup scrolling background
        background = new ScrollingBackground("spaceBackground2.png", 500);

        // Setup SFX and music
        buttonPressSFX = Gdx.audio.newSound((Gdx.files.internal("menuSfx/buttonPress.wav")));

        // Set the size, label and position of the buttons on the main menu screen
        playButton.setSize(w / 2, h / 10);
        playButton.getLabel().setFontScale(3, 3);
        playButton.setPosition(w / 4f, h / 2f);
        upgradeButton.setSize(w / 2, h / 10);
        upgradeButton.getLabel().setFontScale(3, 3);
        upgradeButton.setPosition(w / 4f, (h / 2f) - h / 9 - 10f);
        tutorialButton.setSize(w / 2, h / 10);
        tutorialButton.getLabel().setFontScale(3, 3);
        tutorialButton.setPosition(w / 4f, (h / 2f) - h / 7 - 80f);
        settingsButton.setSize(w / 2, h / 10);
        settingsButton.getLabel().setFontScale(3, 3);
        settingsButton.setPosition(w / 4f, (h / 2f) - (h / 7) * 2 - 60);
        exitButton.setSize(w / 2, h / 10);
        exitButton.getLabel().setFontScale(3, 3);
        exitButton.setPosition(w / 4f, (h / 2f) - (h / 7) * 3 - 40);
        backButton.pad(10);
        backButton.getLabel().setFontScale(1.5f, 1.5f);
        backButton.setSize(w / 6f, h / 16);
        backButton.setPosition(0, h - backButton.getHeight());

        // Add Buttons to stage
        stage.addActor(playButton);
        stage.addActor(upgradeButton);
        stage.addActor(tutorialButton);
        stage.addActor(settingsButton);
        stage.addActor(exitButton);
        stage.addActor(backButton);

        // Add event listeners for each button
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentMenu == 1) {
                    if (saveFile.getSound()) buttonPressSFX.play();
                    dispose();
                    game.setScreen(new LevelSelectScreen(game));
                }
            }
        });
        upgradeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentMenu == 1) {
                    if (saveFile.getSound()) buttonPressSFX.play();
                    dispose();
                    game.setScreen(new UpgradeScreen(game));
                }
            }
        });

        tutorialButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentMenu == 1) {
                    if (saveFile.getSound()) buttonPressSFX.play();
                    dispose();
                    game.setScreen(new TutorialScreen(game));
                }
            }
        });

        settingsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentMenu == 1) {
                    if (saveFile.getSound()) buttonPressSFX.play();
                    showSettings();
                }
            }
        });

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentMenu == 1) {
                    if (saveFile.getSound()) buttonPressSFX.play();
                    dispose();
                    Gdx.app.exit();
                    System.exit(0);
                }
            }
        });

        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentMenu != 1) {
                    if (saveFile.getSound()) buttonPressSFX.play();
                    showMainMenu();
                }
            }
        });

        // Add table to stage for housing settings buttons and visuals
        stage.addActor(settingsTable);
        settingsTable.setSize(w / 1.2f, h / 2f);
        settingsTable.setPosition(w / 13, 90f);

        // Add Music button and graphic to settings table
        settingsTable.add(musicSettingButton).grow().height(h / 10).width(w / 5).pad(10f);
        settingsTable.add(musicImage).row();
        musicSettingButton.getLabel().setFontScale(2, 2);

        // Add Sound button and graphic to settings table
        settingsTable.add(soundSettingButton).grow().height(h / 10).width(w / 5).pad(10f);
        settingsTable.add(soundImage).row();
        soundSettingButton.getLabel().setFontScale(2, 2);

        // Add God Mode Button to settings table. This gives the player insanely high currency.
        settingsTable.add(godModeSettingButton).grow().height(h / 10).colspan(2).pad(10f).row();
        godModeSettingButton.getLabel().setFontScale(2, 2);

        // Add Reset Button to settings table. This resets the players progress and preferences.
        settingsTable.add(resetDataSettingButton).grow().height(h / 10).colspan(2).pad(10f).row();
        resetDataSettingButton.getLabel().setFontScale(2, 2);

        // ADD LISTENERS FOR EACH BUTTON IN THE SETTINGS MENU

        // Toggles the players music preference. Also disables music at the time pressed.
        musicSettingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                if (saveFile.getMusic()) {
                    saveFile.musicOff();
                    musicSettingButton.setText("OFF");
                    game.songPlayer.stop();
                } else {
                    saveFile.musicOn();
                    musicSettingButton.setText("ON");
                    game.songPlayer.playMenuSong();
                }
            }

        });

        // Toggles the players sound preference. Sounds do not play when sound is disabled in the saveFile.
        soundSettingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) {
                    saveFile.soundOff();
                    soundSettingButton.setText("OFF");
                } else {
                    saveFile.soundOn();
                    soundSettingButton.setText("ON");
                    buttonPressSFX.play();
                }
            }

        });

        // Activates God Mode, giving the player insanely high currency for upgrades.
        godModeSettingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                saveFile.godMode();
            }

        });

        // Resets player progress and preferences.
        resetDataSettingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (saveFile.getSound()) buttonPressSFX.play();
                saveFile.reset();
            }

        });


        // Set currently visible menu
        showMainMenu();

        // If mainMenu theme isn't playing, play it with the songPlayer in game.
        if (saveFile.getMusic()) {
            if (!game.songPlayer.menuSong.isPlaying()) {
                game.songPlayer.playMenuSong();
            }
        }

        Gdx.input.setInputProcessor(stage);
    }

    public void update() {
        deltaTime = Gdx.graphics.getDeltaTime();
        background.update(deltaTime);
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update();

        batch.begin();

        background.render(batch);


        batch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        background.dispose();
        stage.dispose();
        batch.dispose();
        buttonPressSFX.dispose();
    }


    /**
     *  This method toggles the visibility of stage actors to show the Settings Menu and hide the
     *  Main Menu.
     */
    public void showSettings() {
        currentMenu = 3;

        playButton.setVisible(false);
        tutorialButton.setVisible(false);
        settingsButton.setVisible(false);
        exitButton.setVisible(false);
        backButton.setVisible(true);
        upgradeButton.setVisible(false);
        musicSettingButton.setVisible(true);
        soundSettingButton.setVisible(true);
        godModeSettingButton.setVisible(true);
        resetDataSettingButton.setVisible(true);
        musicImage.setVisible(true);
        soundImage.setVisible(true);
        godImage.setVisible(true);
        resetImage.setVisible(true);

    }

    /**
     *  Redundant method.
     */
    public void showLevelSelect() {
        currentMenu = 2;
        playButton.setColor(1, 1, 1, 0);
        tutorialButton.setColor(1, 1, 1, 0);
        settingsButton.setColor(1, 1, 1, 0);
        exitButton.setColor(1, 1, 1, 0);
        backButton.setColor(1, 1, 1, 1);
    }

    /**
     *  This method sets visibility of the stage actors to show the main menu and hide the Settings
     *  Menu.
     */
    public void showMainMenu() {
        currentMenu = 1;

        playButton.setVisible(true);
        tutorialButton.setVisible(true);
        settingsButton.setVisible(true);
        exitButton.setVisible(true);
        backButton.setVisible(false);
        upgradeButton.setVisible(true);
        musicSettingButton.setVisible(false);
        soundSettingButton.setVisible(false);
        godModeSettingButton.setVisible(false);
        resetDataSettingButton.setVisible(false);
        musicImage.setVisible(false);
        soundImage.setVisible(false);
        godImage.setVisible(false);
        resetImage.setVisible(false);
    }
}

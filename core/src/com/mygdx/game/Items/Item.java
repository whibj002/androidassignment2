package com.mygdx.game.Items;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.Entities.Sprite;

import java.util.Random;

public abstract class Item extends Sprite {

    protected boolean collected;

    public Item() {
    }

    public abstract Rectangle getBoundingBox();

    public void update() {
        // Better upgrades move faster, harder to catch
        if (this instanceof DamageBoost || this instanceof DefenseBoost || this instanceof VirtualCurrency) {
            setPosition(getPosition().x, getPosition().y - 7);
        } else {
            setPosition(getPosition().x, getPosition().y - 5);
        }

    }

    public boolean isCollected() {
        return collected;
    }

    public void markAsCollected() {
        collected = true;
    }

    protected int random(int start, int finish) {
        Random rand = new Random();
        return rand.nextInt(finish + 1 - start) + start;
    }
}

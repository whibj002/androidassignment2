package com.mygdx.game.Items;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;

public class HealthReducer extends Countable {

    public HealthReducer(int amount) {
        super(amount);
        setCurrentFrame("items/hpReducer.png");
        setWidth(getWidth()/5);
        setHeight(getHeight()/5);
        setPosition(random(50, Gdx.graphics.getWidth() - 100), Gdx.graphics.getHeight() / 1.5f);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 6;
        int yOffset = 12;
        int width = (int) getWidth() -10;
        int height = (int) getHeight() -25;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }
}

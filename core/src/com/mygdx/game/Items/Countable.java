package com.mygdx.game.Items;

public abstract class Countable extends Item {

    private final int amount;

    public Countable(int amount) {
        super();
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
}

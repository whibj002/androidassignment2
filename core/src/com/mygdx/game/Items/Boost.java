package com.mygdx.game.Items;

public abstract class Boost extends Item {
    protected enum BoostState {INACTIVE, ACTIVE, DEPLETED}

    protected BoostState state;
    protected float multiplier;
    protected float duration;


    public Boost(float multiplier, float duration) {
        super();
        this.multiplier = multiplier;
        this.duration = duration;
        state = BoostState.INACTIVE;
    }

    public void activate() {
        if (state != BoostState.DEPLETED) {
            state = BoostState.ACTIVE;
        }
    }

    public void deactivate() {
        state = BoostState.DEPLETED;
    }

    public void update(float deltaTime) {
        super.update();
        if (state == BoostState.ACTIVE) {
            duration -= deltaTime;
        }
        if (duration <= 0) {
            deactivate();
        }
    }

    public float getMultiplier() {
        return multiplier;
    }

    public float getDuration() {
        return duration;
    }

    public boolean isInactive() {
        return state == BoostState.INACTIVE;
    }

    public boolean isActive() {
        return state == BoostState.ACTIVE;
    }

    public boolean isDepleted() {
        return state == BoostState.DEPLETED;
    }

}

package com.mygdx.game.Items;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;

public class Health extends Countable {

    public Health(int amount) {
        super(amount);
        setCurrentFrame("items/hpBoost.png");
        setWidth(getWidth()/5);
        setHeight(getHeight()/5);
        setPosition(random(50, Gdx.graphics.getWidth() - 100), Gdx.graphics.getHeight() / 1.5f);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 0;
        int yOffset = 0;
        int width = (int) getWidth();
        int height = (int) getHeight();
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }
}

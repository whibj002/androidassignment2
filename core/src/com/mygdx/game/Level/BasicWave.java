package com.mygdx.game.Level;

import com.mygdx.game.Entities.BasicEnemy;
import com.mygdx.game.Entities.DoubleShot;
import com.mygdx.game.Entities.HoleEnemy;
import com.mygdx.game.Entities.IonEnemy;
import com.mygdx.game.Entities.LaserEnemy;
import com.mygdx.game.Entities.ParticleSystem;
import com.mygdx.game.Entities.SplitShotEnemy;

public class BasicWave extends Wave {

    public BasicWave(int numOfEnemies, boolean itemDrop) {
        super(numOfEnemies, itemDrop);
    }

    @Override
    protected void setUp(ParticleSystem p) {
        int splitHoleCount = 0;
        int doubleShotCount = 0;
        boolean sideEntry = false;

        for (int i = 0; i < numOfEnemies; i++) {
            int random = random(1, 6);

            if (random == 1 && splitHoleCount < 1) {
                enemies[i] = new SplitShotEnemy(p);
                splitHoleCount++;
            } else if (random == 2 && splitHoleCount < 1) {
                enemies[i] = new HoleEnemy(p);
                splitHoleCount++;
            } else if (random == 3 && doubleShotCount < 1) {
                enemies[i] = new DoubleShot(p);
                doubleShotCount++;
                sideEntryIndex = i;
                sideEntry = true;
            } else if (random == 4) {
                enemies[i] = new LaserEnemy(p);
            }  else if (random == 5) {
                enemies[i] = new IonEnemy(p);
            }  else {
                enemies[i] = new BasicEnemy(p);
            }
        }
        if (!sideEntry) {
            sideEntryIndex = Integer.MAX_VALUE;
        }
        if (itemDrop) {
            item = itemList[random(0,4)];
        }
        generateLocations();
    }
}

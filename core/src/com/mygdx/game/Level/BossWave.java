package com.mygdx.game.Level;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.Entities.BigBoss;
import com.mygdx.game.Entities.BulletHellBoss;
import com.mygdx.game.Entities.Enemy;
import com.mygdx.game.Entities.ParticleSystem;

public class BossWave extends Wave {
    Enemy.EnemyType type;

    public BossWave(Enemy.EnemyType bossType) {
        super(1, true);
        type = bossType;
    }

    @Override
    protected void setUp(ParticleSystem p) {
        if (type == Enemy.EnemyType.BIG_BOSS) {
            enemies[0] = new BigBoss(p);
        } else if (type == Enemy.EnemyType.BULLET_HELL_BOSS) {
            enemies[0] = new BulletHellBoss(p);
        }
        if (itemDrop) {
            item = itemList[random(0, 4)];
        }
        generateLocations();
    }

    @Override
    protected void generateLocations() {
        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();
        if (enemies != null && enemies[0] != null) {
            float x = w / 2f - (int) enemies[0].getWidth() / 2f;
            float initialY = h * 1.5f;
            enemies[0].setPosition(x, initialY);
        }
    }
}

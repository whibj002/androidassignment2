package com.mygdx.game.Level;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Entities.ParticleSystem;

public class Level {
    public enum levelState {LOAD_WAVE, RUN_WAVE, FINISHED}

    private levelState currentState;

    private Wave[] waves;
    private ParticleSystem particleSystem;

    private int currentWave;
    private int totalWaves;

    public Level(ParticleSystem particleSystem) {
        this.particleSystem = particleSystem;
    }

    public void setWaves(Wave[] waves) {
        this.waves = waves;
        totalWaves = waves.length;
        currentWave = 0;
    }

    public void initialise() {
        loadNextWave();
    }

    private void loadNextWave() {
        if (currentWave < totalWaves) {
            waves[currentWave].setUp(particleSystem);
            particleSystem.enemiesThisRound = waves[currentWave].getNumOfEnemies();
            particleSystem.enemies = waves[currentWave].getEnemies();
            currentState = levelState.RUN_WAVE;
        }
    }

    public boolean isFinished() {
        return currentState == levelState.FINISHED;
    }


    public void update(float deltaTime, Vector2 playerPos, long currentTime) {
        switch (currentState) {
            case FINISHED: {
                break;
            }
            case LOAD_WAVE: {
                loadNextWave();
            }
            break;
            case RUN_WAVE: {
                waves[currentWave].update(deltaTime, playerPos, currentTime);
                if (waves[currentWave].isCleared()) {
                    currentWave++;
                    if (currentWave >= totalWaves) {
                        currentState = levelState.FINISHED;
                    } else {
                        currentState = levelState.LOAD_WAVE;
                    }
                }
            }
            break;
        }
    }

    public Wave getCurrentWave() {
        if (currentWave < totalWaves) {
            return waves[currentWave];
        }
        return null;
    }

    public void render(SpriteBatch batch) {
        if (currentWave < totalWaves) {
            waves[currentWave].render(batch);
        }
    }

    public boolean isLastWave() {
        return currentWave == totalWaves;
    }
}


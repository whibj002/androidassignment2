package com.mygdx.game.Level;

import com.mygdx.game.Entities.BasicEnemy;
import com.mygdx.game.Entities.EliteBasicEnemy;
import com.mygdx.game.Entities.EliteLaserEnemy;
import com.mygdx.game.Entities.Enemy;
import com.mygdx.game.Entities.IonEnemy;
import com.mygdx.game.Entities.LaserEnemy;
import com.mygdx.game.Entities.ParticleSystem;

public class TypeWave extends Wave {
    Enemy.EnemyType enemyType;

    public TypeWave(int numOfEnemies, Enemy.EnemyType enemyType, boolean itemDrop) {
        super(numOfEnemies, itemDrop);
        this.enemyType = enemyType;
        sideEntryIndex = Integer.MAX_VALUE;
    }

    @Override
    protected void setUp(ParticleSystem p) {
        for (int i = 0; i < numOfEnemies; i++) {
            switch (enemyType) {
                case BASIC:
                    enemies[i] = new BasicEnemy(p);
                    break;
                case ELITE_BASIC:
                    enemies[i] = new EliteBasicEnemy(p);
                    break;
                case LASER:
                    enemies[i] = new LaserEnemy(p);
                    break;
                case ELITE_LASER:
                    enemies[i] = new EliteLaserEnemy(p);
                    break;
                case ION:
                    enemies[i] = new IonEnemy(p);
                    break;
            }
            if (itemDrop) {
                item = itemList[random(0,4)];
            }
            generateLocations();
        }
    }
}

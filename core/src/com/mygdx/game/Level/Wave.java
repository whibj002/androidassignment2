package com.mygdx.game.Level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Entities.Enemy;
import com.mygdx.game.Entities.ParticleSystem;
import com.mygdx.game.Items.DamageBoost;
import com.mygdx.game.Items.DefenseBoost;
import com.mygdx.game.Items.Health;
import com.mygdx.game.Items.HealthReducer;
import com.mygdx.game.Items.Item;
import com.mygdx.game.Items.VirtualCurrency;

import java.util.Random;

public abstract class Wave {

    protected int numOfEnemies;
    protected Enemy[] enemies;
    protected boolean itemDrop;
    protected boolean cleared;
    protected Item[] itemList;
    protected Item item;
    //The array index of an enemy that spawns from the side of the screen rather than the bottom  (max 1 per wave).
    protected int sideEntryIndex;

    public Wave(int numOfEnemies, boolean itemDrop) {
        this.numOfEnemies = numOfEnemies;
        this.itemDrop = itemDrop;
        enemies = new Enemy[numOfEnemies];
        itemList = new Item[]{new Health(25), new HealthReducer(10), new DamageBoost(1, 15), new DefenseBoost(1, 15), new VirtualCurrency(5000)};
    }

    protected abstract void setUp(ParticleSystem p);

    protected void generateLocations() {
        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();

        if (enemies != null && enemies[0] != null) {
            int centerX = w / 2 - (int) enemies[0].getWidth() / 2;
            //initialHeight controls how long it takes for a wave to start.
            int initialHeight = h * 2;
            int distance = 75;
            Vector2 initSpawn = new Vector2(centerX, initialHeight);
            Vector2 leftSpawn = new Vector2(centerX, initialHeight);
            Vector2 rightSpawn = new Vector2(centerX, initialHeight);

            for (int i = 0; i < numOfEnemies; ++i) {
                if (enemies[i] != null)
                    //Sets specific values if there's an enemy from the side.
                    if (i == sideEntryIndex && enemies[i].getEnemyType() != Enemy.EnemyType.HOLE) {
                        enemies[i].setPosition(w * 2.5f, h * 0.60f);
                    } else if (i % 2 == 0) {
                        if (i == 0) {
                            enemies[i].setPosition(initSpawn.x, initSpawn.y);
                        } else {
                            leftSpawn.x -= distance;
                            leftSpawn.y -= distance;
                            enemies[i].setPosition(leftSpawn.x, leftSpawn.y);
                        }
                    } else {
                        rightSpawn.x += distance;
                        rightSpawn.y -= distance;
                        enemies[i].setPosition(rightSpawn.x, rightSpawn.y);
                    }
            }
        }
    }

    public void update(float deltaTime, Vector2 playerPos, long currentTime) {
        int deadEnemies = 0;

        //Remove dead enemies, update the alive ones.
        for (int i = 0; i < numOfEnemies; i++) {
            if (enemies[i] != null) {
                if (enemies[i].isDead()) {
                    enemies[i].dispose();
                    enemies[i] = null;
                    deadEnemies++;
                } else {
                    enemies[i].update(deltaTime, playerPos, currentTime);
                }
            } else {
                deadEnemies++;
            }
        }
        if (deadEnemies == numOfEnemies) {
            cleared = true;
        }
    }

    public void render(SpriteBatch batch) {
        for (int i = 0; i < numOfEnemies; i++) {
            if (enemies[i] != null) {
                enemies[i].render(batch);
            }
        }
    }

    public boolean isCleared() {
        return cleared;
    }

    public Enemy[] getEnemies() {
        return enemies;
    }

    public int getNumOfEnemies() {
        return numOfEnemies;
    }

    public Item getItem() {
        if (item != null && !item.isCollected()) {
            return item;
        }
        return null;
    }

    protected int random(int start, int finish) {
        Random rand = new Random();
        return rand.nextInt(finish + 1 - start) + start;
    }
}

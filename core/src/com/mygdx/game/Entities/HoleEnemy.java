package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class HoleEnemy extends Enemy {
    private enum MOVE_STATE {NONE, CHOOSE_DEST, MOVE}

    private long lastTime = 0;
    private long moveLastTime = 0;

    private float movementSpeed = 350f;
    private float reloadTime = 6000f;
    private float shotSpeed = 110f;
    private float shotLifetime = 1.5f;
    private float dodgeSpeed = 250f;
    private float moveTime = 1200f;

    private Vector2 currentPlayerPosition;
    private Vector2 moveVelocity;

    private MOVE_STATE moveState;

    public HoleEnemy(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.HOLE;

        setCurrentFrame("enemyShipCustom6.png");
        setHeight(128.0f);
        setWidth(128.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion2.png", 1, 8, 0.05f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/holeEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        currentPlayerPosition = new Vector2();
        moveVelocity = new Vector2();

        health = 150;
        damageOutput = 80;
        defense = 1;
        stateTime = 0;

        moveState = MOVE_STATE.NONE;
        setCurrentState(EnemyState.MOVING);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 38;
        int yOffset = 30;
        int width = (int) getWidth() - 75;
        int height = (int) getHeight() - 70;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public Vector2 getScreenPosition() {
        Vector2 position = new Vector2(super.position.x, super.position.y);

        position.x += 240;
        position.y += 290;


        return position;
    }

    public void fire() {
        Gdx.app.log("Firing:", "True");
        Vector2 firePosition = new Vector2(getPosition().x + getWidth() / 2 - 50, getPosition().y);
        //Vector2 currentTarget = new Vector2(currentPlayerPosition);
        Vector2 velocity = (new Vector2(0, -1).scl(shotSpeed));

        //Gdx.app.log("Basic enemy target x:", Float.toString(currentTarget.x));
        //Gdx.app.log("Basic enemy target y:", Float.toString(currentTarget.y));

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.HOLE_BULLET, firePosition, 100, 100, velocity, shotLifetime, damageOutput);
        if (sound) fireSound.play();

    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom6_DMG.png");
            setHeight(128.0f);
            setWidth(128.0f);
        }

        if (sound) hurtSound.play();

        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            moveState = MOVE_STATE.NONE;
            if (sound) dieSound.play();
        }
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom6.png");
                    setHeight(128.0f);
                    setWidth(128.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        switch (currentState) {
            case MOVING: {
                move(0, -movementSpeed * deltaTime);
                if (getPosition().y <= screenHeight- (height * 0.85)) {
                    isVulnerable = true;
                    currentState = EnemyState.IDLE;
                    moveState = MOVE_STATE.CHOOSE_DEST;
                }
            }
            break;
            case IDLE: {
                //currentPlayerPosition = new Vector2(playerPosition.x, playerPosition.y);
                currentState = EnemyState.FIRE;

            }
            break;
            case RELOAD: {
                //Gdx.app.log("Reloading State:", "True");
                if (currentTime - lastTime >= reloadTime) {
                    lastTime = currentTime;
                    //currentPlayerPosition = new Vector2(playerPosition);
                    currentState = EnemyState.FIRE;
                }
            }
            break;
            case FIRE: {
                Gdx.app.log("Firing State:", "True");
                fire();
                lastTime = currentTime;
                currentState = EnemyState.RELOAD;

            }
            break;
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }


        switch (moveState) {
            case CHOOSE_DEST: {
                /*randomX = random((int) width, screenWidth * 2);
                randomY = (int)(screenHeight - height);

                Vector2 destination = new Vector2(randomX, randomY);

                moveVelocity = getScreenPosition().sub(destination).nor().scl(-dodgeSpeed);
                moveLastTime = currentTime;
                moveState = MOVE_STATE.MOVE;*/
                move(-dodgeSpeed * deltaTime, 0);

                if (getBoundingBox().x <= 0.1 * screenWidth)
                    moveState = MOVE_STATE.MOVE;
            }
            break;
            case MOVE: {
                /*if (currentState != EnemyState.MOVING) {
                    move(moveVelocity.x * deltaTime, 0);
                    if (position.x <= 0 || position.x + width > screenWidth || position.y < 0 || position.y + height > screenHeight) {
                        moveState = MOVE_STATE.CHOOSE_DEST;
                    }
                    if (currentTime - moveLastTime >= moveTime)
                        moveState = MOVE_STATE.CHOOSE_DEST;
                }*/

                move(dodgeSpeed * deltaTime, 0);

                if (getBoundingBox().x + getBoundingBox().getWidth() >= 0.9 * screenWidth)
                    moveState = MOVE_STATE.CHOOSE_DEST;
            }
            break;
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }

}

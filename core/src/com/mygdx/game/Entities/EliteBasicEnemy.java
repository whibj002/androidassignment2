package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class EliteBasicEnemy extends Enemy {

    private float ySpeed = 300.0f;
    private float xSpeed = 0;

    // PLay around with these!! enemy fire controls
    private float reloadTime = 500f;
    private float fireRate = 200f;
    private float shotSpeed = 350f;
    private int damage = 25;
    private int clipCapacity = 1;

    private long lastTime = 0;

    private Vector2 target;

    public EliteBasicEnemy(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.ELITE_BASIC;

        setCurrentFrame("enemyShipCustom3Elite.png");
        setHeight(128.0f);
        setWidth(128.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/standardEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/eliteExplosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        health = 250;
        defense = 1;
        stateTime = 0;

        setCurrentState(EnemyState.MOVING);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 50;
        int yOffset = 40;
        int width = (int) getWidth() - 100;
        int height = (int) getHeight() - 70;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public void fire() {
        Vector2 firePosition = new Vector2(getPosition().x + getWidth() / 2 - 22, getPosition().y);
        Vector2 currentTarget = new Vector2(target);
        Vector2 velocity = (currentTarget.sub(firePosition).nor().scl(shotSpeed));

        //Gdx.app.log("Basic enemy target x:", Float.toString(currentTarget.x));
        //Gdx.app.log("Basic enemy target y:", Float.toString(currentTarget.y));

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, firePosition, 50, 50, velocity, 3.0f, damage);
        lastTime = System.currentTimeMillis();
        --clipCapacity;
        if (sound) fireSound.play();
    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom3Elite_DMG.png");
            setHeight(128.0f);
            setWidth(128.0f);
        }

        if (sound) hurtSound.play();

        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            if (sound) dieSound.play();
        }
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom3Elite.png");
                    setHeight(128.0f);
                    setWidth(128.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        switch (currentState) {
            case MOVING: {
                move(xSpeed * deltaTime, -ySpeed * deltaTime);
                if (getPosition().y <= screenHeight * 0.75) {
                    isVulnerable = true;
                    setCurrentState(EnemyState.IDLE);
                }
            }
            break;
            case IDLE: {
                lastTime = currentTime;
                setCurrentState(EnemyState.RELOAD);
            }
            break;
            case RELOAD: {
                if (currentTime - lastTime >= reloadTime) {
                    clipCapacity = 5;
                    target = new Vector2(playerPosition);
                    //Gdx.app.log("Basic enemy target x:", Float.toString(target.x));
                    //Gdx.app.log("Basic enemy target y:", Float.toString(target.y));
                    fire();
                    setCurrentState(EnemyState.FIRE);
                }
            }
            break;
            case FIRE: {
                if (clipCapacity <= 0)
                    setCurrentState(EnemyState.IDLE);
                if (currentTime - lastTime >= fireRate)
                    fire();
            }
            break;
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }

}

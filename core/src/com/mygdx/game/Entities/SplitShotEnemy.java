package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class SplitShotEnemy extends Enemy {
    private enum MOVE_STATE {NONE, DODGING, MOVE_LEFT, MOVE_RIGHT}

    private float ySpeed = 300.0f;
    private float xSpeed = 0;
    private float hSpeed = 250.0f;
    private float reloadTime = 3500f;
    private float shotSpeed = 150f;
    private float shotLifetime = 3f;
    private int damage = 80;

    private long lastTime = 0;

    private MOVE_STATE currentMoveState = MOVE_STATE.NONE;

    public SplitShotEnemy(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.SPLIT_SHOT;

        setCurrentFrame("enemyShipCustom1.png");
        setHeight(90.0f);
        setWidth(90.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/scatterEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        health = 200;
        defense = 1;
        damageOutput = 250;
        stateTime = 0;
        setCurrentState(EnemyState.MOVING);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 10;
        int yOffset = 30;
        int width = (int) getWidth() - 20;
        int height = (int) getHeight() - 40;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public void fire() {
        //Gdx.app.log("Firing:", "true");
        Vector2 firePosition = new Vector2(getPosition().x - 125, getPosition().y - 100);
        Vector2 velocity = (new Vector2(0, -1)).scl(shotSpeed);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET2, firePosition, 350f, 200f, velocity, shotLifetime, damage);
        //particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PELLET, firePosition, 100f, 100f, velocity, 3, 200);
        if (sound) fireSound.play();
    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom1_DMG.png");
            setHeight(90.0f);
            setWidth(90.0f);
        }

        if (sound) hurtSound.play();

        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            if (sound) dieSound.play();
        }
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer -= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom1.png");
                    setHeight(90.0f);
                    setWidth(90.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        switch (currentMoveState) {
            case MOVE_LEFT: {
                move(-hSpeed * deltaTime, 0);

                if (getBoundingBox().x <= 0.1 * screenWidth)
                    currentMoveState = MOVE_STATE.MOVE_RIGHT;
            }
            break;
            case MOVE_RIGHT: {
                move(hSpeed * deltaTime, 0);

                if (getBoundingBox().x + getBoundingBox().getWidth() >= 0.9 * screenWidth)
                    currentMoveState = MOVE_STATE.MOVE_LEFT;
            }
            break;
        }


        switch (currentState) {
            case MOVING: {
                move(xSpeed * deltaTime, -ySpeed * deltaTime);
                if (getPosition().y <= screenHeight - height) {
                    isVulnerable = true;
                    lastTime = currentTime;
                    setCurrentState(EnemyState.RELOAD);
                    currentMoveState = MOVE_STATE.MOVE_LEFT;
                }
            }
            break;
            case IDLE: {


            }
            break;
            case RELOAD: {
                if (currentTime - lastTime >= reloadTime) {
                    currentState = EnemyState.FIRE;
                }

            }
            break;
            case FIRE: {
                fire();
                lastTime = currentTime;
                currentState = EnemyState.RELOAD;

            }
            break;
            case EXPLODING: {
                currentMoveState = MOVE_STATE.NONE;

                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;
            }
            break;
            case DEAD: {

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }
    }


    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }
}

package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Goon extends Enemy {
    public enum MOVE_STATE {NONE, ORBIT}

    private MOVE_STATE moveState;

    private Vector2 position;

    private long lastTime = 0;

    public float reloadTime = 4000f;
    private Vector2 firePosition;
    private Vector2 fireVelocity;
    private float shotSpeed = 550f;
    private int fireCap = 10;
    private float fireRate = 250f;
    private float bulletLifetime = 1;

    private Vector2 distance;
    private Vector2 origin;


    private float _originX;
    private float _originY;

    public Goon(ParticleSystem p, float originX, float originY, int distanceFromOrigin) {
        super(p);
        enemyType = EnemyType.GOON;
        _originX = originX;
        _originY = originY;

        setCurrentFrame("goon.png");
        setHeight(128.0f);
        setWidth(128.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);

        health = 100;
        defense = 1;
        damageOutput = 15;
        stateTime = 0;
        isVulnerable = true;

        distance = new Vector2(0, distanceFromOrigin);
        origin = new Vector2(_originX, _originY);

        moveState = MOVE_STATE.NONE;

        firePosition = new Vector2();
        fireVelocity = new Vector2();

        position = new Vector2();
        setCurrentState(EnemyState.MOVING);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 225;
        int yOffset = 275;
        int width = (int) getWidth() - 100;
        int height = (int) getHeight() - 70;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public Vector2 getScreenPosition() {
        position.x = super.position.x + 30;
        position.y = super.position.y + 15;

        position.x += 200;
        position.y += 280;


        return position;
    }

    public void fire() {
        firePosition.x = getPosition().x + getWidth() / 2 - 18;
        firePosition.y = getPosition().y + 40;

        fireInDirection(0, -1);

        fireInDirection(1, 0);

        fireInDirection(0, 1);

        fireInDirection(-1, 0);

        --fireCap;
    }

    private void fireInDirection(float directionX, float directionY) {
        fireVelocity.x = directionX;
        fireVelocity.y = directionY;
        fireVelocity.scl(shotSpeed);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, fireVelocity, bulletLifetime, damageOutput);
    }


    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);


        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            moveState = MOVE_STATE.NONE;

        }
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;


        switch (currentState) {
            case MOVING: {
                //move(-movementSpeed * deltaTime, 0);
                //if (getScreenPosition().dst2(origin) >= distanceFromOrigin * distanceFromOrigin) {
                setCurrentState(EnemyState.IDLE);
                moveState = MOVE_STATE.ORBIT;

                //}

            }
            break;
            case IDLE: {
                lastTime = currentTime;
                setCurrentState(EnemyState.RELOAD);
            }
            break;
            case RELOAD: {
                if (currentTime - lastTime >= reloadTime) {
                    lastTime = currentTime;
                    currentState = EnemyState.FIRE;
                    fireCap = 20;
                }
            }
            break;
            case FIRE: {
                if (currentTime - lastTime >= fireRate) {
                    fire();
                    lastTime = currentTime;
                }

                if (fireCap <= 0)
                    currentState = EnemyState.RELOAD;

            }
            break;
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }

        switch (moveState) {
            case ORBIT: {
                origin.set(_originX, _originY);
                distance.rotateDeg(50 * deltaTime);
                Vector2 orbitPosition = origin.add(distance);
                setPosition(orbitPosition.x, orbitPosition.y);

                /*int radius = 10;
                double radians = Math.toRadians(45f);
                double xOrbit = origin.x + radius * Math.cos(radians);
                double yOrbit = origin.y + radius * Math.sin(radians);

                setPosition((float)xOrbit, (float)yOrbit);*/
            }
            break;
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }
}

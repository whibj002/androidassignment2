package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.mygdx.game.Items.DamageBoost;
import com.mygdx.game.Items.DefenseBoost;
import com.mygdx.game.Items.Health;
import com.mygdx.game.Items.HealthReducer;
import com.mygdx.game.Items.Item;
import com.mygdx.game.Items.VirtualCurrency;
import com.mygdx.game.SaveFile;

import java.util.ArrayList;

public class Player extends Sprite {
    public enum PlayerState {IDLE, MOVING, DEAD, EXPLODING}

    //public enum LaserHit
    public enum FireState {ACTIVE, DISABLED}

    public Vector2 playerDelta;

    private PlayerState currentState;
    public FireState fireState;
    private float disabledFireTime = 2500f;

    private ParticleSystem particleSystem;

    private Polygon boundingPoly;

    private long lastTime;

    private SaveFile saveFile;
    private boolean sound;

    private int maxHealth;
    private int health;
    private float defense;
    private int damageOutput;
    private float fireRate;
    private float shotSpeed;
    private float shotLifeTime = 12f;
    private int projectiles;
    private int virtualCurrency;

    private ArrayList<DamageBoost> damageBoosts;
    private ArrayList<DefenseBoost> defenseBoosts;

    private int damageMultiplier;
    private int defenseMultiplier;

    private float regenCooldown;

    private boolean looping = false;
    private float stateTime;
    private Animation currentAnimation;
    private Animation deathAnimation;

    private Sound fireSound;
    private Sound dieSound;
    private Sound hurtSound;
    private Sound healSound;
    private Sound powerUpSound;

    private boolean DMGboostActive;
    private boolean DEFboostActive;
    private float currentBoostDuration;

    public Player(ParticleSystem p) {
        particleSystem = p;
        saveFile = new SaveFile();
        updateFromSaveFile();

        setCurrentFrame("playerShipCustom.png");
        setHeight(80.0f);
        setWidth(75.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("playerDeath.png", 1, 12, 0.05f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/playerFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/playerDie.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/playerHit.wav")));
        healSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/health.wav")));
        powerUpSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/powerUp.wav")));

        //health = 10000;

        playerDelta = new Vector2();
        damageBoosts = new ArrayList<>();
        defenseBoosts = new ArrayList<>();
        damageMultiplier = 1;
        defenseMultiplier = 1;

        boundingPoly = new Polygon(new float[]{0, 0, 0, 0, 0, 0});
        setBoundingPoly();

        stateTime = 0;
        lastTime = System.currentTimeMillis();

        fireState = FireState.ACTIVE;
        regenCooldown = 1f;
    }

    @Override
    public void dispose() {
        super.dispose();
        fireSound.dispose();
        dieSound.dispose();
        hurtSound.dispose();
        healSound.dispose();
        powerUpSound.dispose();
    }

    public PlayerState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(PlayerState state) {
        currentState = state;
    }

    public Rectangle getBoundingBox() {
        int xOffset = 13;
        int yOffset = 0;
        int width = (int) getWidth() -26;
        int height = (int) getHeight() -35;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public void setBoundingPoly() {
        Rectangle r = this.getBoundingBox();
        boundingPoly = new Polygon(new float[]{0, 0,
                r.getWidth(), 0,
                r.getWidth(), r.getHeight(),
                0, r.getHeight()
        });
        boundingPoly.setOrigin(r.getWidth() / 2, r.getHeight() / 2);
        boundingPoly.setScale(1, 1);
    }

    public Polygon getBoundingPoly() {
        boundingPoly.setPosition(getScreenPosition().x - getBoundingBox().width / 2,
                getScreenPosition().y - getBoundingBox().height/2 - 17);

        return boundingPoly;
    }

    public void fire() {
        Vector2 firePosition;
        Vector2 velocity;

        switch (projectiles){
            case 1:
                // Single bullet from middle of player ship
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 25, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, shotLifeTime, damageOutput * damageMultiplier);
                break;
            case 2:
                // Two bullets, each fire position is slightly off center from player
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 10, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 40, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                break;
            case 3:
                // Single bullet from middle of player ship
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 25, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                // One bullet, veering to the right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 10, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(-4);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                // One bullet, veering to the right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 40, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(4);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                break;
            case 4:
                // Two bullets, each fire position is slightly off center from player
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 15, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 35, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                // One bullet, veering to the right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 10, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(-5);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                // One bullet, veering to the right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 40, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(5);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                break;
            case 5:
                // Three bullets, each fire position is slightly off center from player
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 10, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 40, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 25, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                // One bullet, veering to the right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 10, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(-5);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);

                // One bullet, veering to the left
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 40, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(5);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                break;
            case 6:
                // Slightly veering to right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 20, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(-1);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                // Two bullets, veering to the right
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 10, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(-4);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                firePosition = new Vector2(getPosition().x + getWidth() / 2, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(-7);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                // One bullet, veering to the left
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 30, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(1);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                // Two to the left
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 40, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(4);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                firePosition = new Vector2(getPosition().x + getWidth() / 2 - 50, getPosition().y + getHeight() - 30);
                velocity = (new Vector2(0, 1)).scl(shotSpeed).rotateDeg(7);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.PLAYER_BULLET1, firePosition, 50, 50, velocity, 2.0f, damageOutput * damageMultiplier);
                break;
            default:
                Gdx.app.log("Error: ", "Issue with Player fire()");
                break;
        }
        if (sound)  fireSound.play();
    }

    public Vector2 getScreenPosition() {
        return new Vector2(super.position.x + width/2, super.position.y + height/2);
    }

    public static Vector2 getScreenPosition(float x, float y) {
        return new Vector2(x + 40, y + 32.5f);
    }

    public void disableFire() {
        if (fireState != FireState.DISABLED) fireState = FireState.DISABLED;
    }

    public void update(float deltaTime, long currentTime, float moveX, float moveY) {
        stateTime += deltaTime;
        damageMultiplier = 1;
        defenseMultiplier = 1;

        playerDelta.x = moveX;
        playerDelta.y = moveY;

        if (DMGboostActive){
            damageMultiplier = 2;
            if (currentBoostDuration > 0.0f){
                currentBoostDuration -= deltaTime;
            }
            else {
                damageMultiplier = 1;
                DMGboostActive = false;
            }
        }
        else if (DEFboostActive){
            defenseMultiplier = 2;
            if (currentBoostDuration > 0.0f){
                currentBoostDuration -= deltaTime;
            }
            else {
                defenseMultiplier = 1;
                DEFboostActive = false;
            }
        }

        switch (currentState) {
            case MOVING: {
                //Health regeneration
                regenCooldown -= deltaTime;

                if (regenCooldown <= 0) {
                    heal(1);
                    regenCooldown = 1f;
                }

                //Boosts update
                for (int i = Math.max(damageBoosts.size(), defenseBoosts.size()) - 1; i >= 0; i--) {
                    if (i < damageBoosts.size()) {
                        DamageBoost db = damageBoosts.get(i);
                        db.update(deltaTime);
                        if (db.isActive()) {
                            damageMultiplier += db.getMultiplier();
                        } else if (db.isInactive()) {
                            damageBoosts.remove(i);
                        }
                    }
                    if (i < defenseBoosts.size()) {
                        DefenseBoost db = defenseBoosts.get(i);
                        db.update(deltaTime);
                        if (db.isActive()) {
                            defenseMultiplier += db.getMultiplier();
                        } else if (db.isInactive()) {
                            defenseBoosts.remove(i);
                        }
                    }
                }


                move(playerDelta.x, playerDelta.y);

                switch (fireState) {
                    case ACTIVE: {
                        if (currentTime - lastTime >= fireRate) {
                            fire();
                            lastTime = currentTime;
                        }
                    }
                    break;
                    case DISABLED: {
                        if (currentTime - lastTime >= disabledFireTime) {
                            lastTime = currentTime;
                            fireState = FireState.ACTIVE;
                        }
                    }
                    break;
                }

            }
            break;
            case DEAD: {
                damageBoosts.clear();
                defenseBoosts.clear();
                currentAnimation = deathAnimation;
                looping = false;
            }
            break;
        }
    }

    public void render(SpriteBatch batch) {
        if (currentAnimation != null) {
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));
        }
        super.render(batch);
    }

    public void takeDamage(int amount) {
        if (currentState != PlayerState.DEAD) {
            amount /= defense * defenseMultiplier;
            if (health - amount <= 0) {
                health = 0;
                currentState = PlayerState.DEAD;
                stateTime = 0;
                setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
                if (sound) dieSound.play();
            } else {
                health -= amount;
                if (sound) hurtSound.play();
            }
        }
    }

    public void collectItem(Item item) {
        item.markAsCollected();

        if (item instanceof Health) {
            heal((Health) item);
            if (sound) healSound.play();
        } else if (item instanceof HealthReducer) {
            reduceHealth((HealthReducer) item);
            if (sound) hurtSound.play();
        } else if (item instanceof DamageBoost) {
            addDamageBoost((DamageBoost) item);
            if (sound) powerUpSound.play();
        } else if (item instanceof DefenseBoost) {
            addDefenseBoost((DefenseBoost) item);
            if (sound) powerUpSound.play();
        } else if (item instanceof VirtualCurrency) {
            addVirtualCurrency((VirtualCurrency) item);
            if (sound) healSound.play();
        }
    }

    private void addDamageBoost(DamageBoost damageBoost) {
        damageBoost.activate();
        damageBoosts.add(damageBoost);
    }

    private void addDefenseBoost(DefenseBoost defenseBoost) {
        defenseBoost.activate();
        defenseBoosts.add(defenseBoost);
    }

    private void heal(Health healthItem) {
        if (currentState != PlayerState.DEAD) {
            if (health + healthItem.getAmount() > maxHealth) {
                health = maxHealth;
            } else {
                health += healthItem.getAmount();
            }
        }
    }

    private void reduceHealth(HealthReducer healthReducer) {
        if (health - healthReducer.getAmount() / (defense * defenseMultiplier) < 0) {
            health = 0;
            currentState = PlayerState.DEAD;
        } else {
            health -= healthReducer.getAmount() / (defense * defenseMultiplier);
        }
    }

    private void addVirtualCurrency(VirtualCurrency vc) {
        virtualCurrency += vc.getAmount();
        saveFile.increaseVC(vc.getAmount());
    }

    public void heal(int amount) {
        if (currentState != PlayerState.DEAD) {
            if (health + amount > maxHealth) {
                maxHealth();
            } else {
                health += amount;
            }
        }
    }

    public int getDamageMultiplier() {
        return damageMultiplier;
    }

    public int getDefenseMultiplier() {
        return defenseMultiplier;
    }

    public void maxHealth() {
        health = maxHealth;
    }

    public int getHealth() {
        return health;
    }

    public boolean isDead() {
        return health <= 0;
    }

    public int getVirtualCurrency() {
        return virtualCurrency;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getDamageOutput() {
        return damageOutput;
    }

    public void updateFromSaveFile() {
        maxHealth = saveFile.getHealth();
        health = maxHealth;
        damageOutput = saveFile.getDamage();
        defense = saveFile.getDefense();
        shotSpeed = saveFile.getShotSpeed();
        fireRate = saveFile.getFireRate();
        projectiles = saveFile.getProjectiles();
        virtualCurrency = saveFile.getVC();
        sound = saveFile.getSound();
    }

    public void activateDamageBoost(float duration){
        DMGboostActive = true;
        currentBoostDuration = duration;
    }

    public void activateDefenceBoost(float duration){
        DEFboostActive = true;
        currentBoostDuration = duration;
    }
}

package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.SaveFile;

public class BasicEnemy extends Enemy {

    // Enemy movement speeds
    private float ySpeed = 300.0f;
    private float xSpeed = 0;

    // PLay around with these!! enemy fire controls
    private float reloadTime = 500f;
    private float fireRate = 200f;

    // Bullet speeds
    private float shotSpeed = 350f;
    private float shotLifetime = 2.5f;


    private int damage = 25;
    private int clipCapacity = 3;

    private long lastTime = 0;

    private Vector2 target;

    public BasicEnemy(ParticleSystem p) {
        super(p);

        // Set the enemy time
        enemyType = EnemyType.BASIC;

        // Load tecture
        setCurrentFrame("enemyShipCustom3.png");
        setHeight(128.0f);
        setWidth(128.0f);

        // Animations
        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);

        // sounds
        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/standardEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        health = 100;
        defense = 1;
        stateTime = 0;

        setCurrentState(EnemyState.MOVING);
    }


    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 50;
        int yOffset = 40;
        int width = (int) getWidth() - 100;
        int height = (int) getHeight() - 70;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public void fire() {
        // Position where enemy fires from
        Vector2 firePosition = new Vector2(getPosition().x + getWidth() / 2 - 22, getPosition().y);

        // Where to shoot
        Vector2 currentTarget = new Vector2(target);

        // calculate the velocity of the shot
        Vector2 velocity = (currentTarget.sub(firePosition).nor().scl(shotSpeed));

        // Call the ParticleSystem's spawn function to spawn the bullet
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, firePosition, 50, 50, velocity, shotLifetime, damage);
        lastTime = System.currentTimeMillis();

        // Decrease clip capacity
        --clipCapacity;
        if (sound) {
            fireSound.play();
        }
    }


    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        // Make enemy flash red when hurt
        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom3_DMG.png");
            setHeight(128.0f);
            setWidth(128.0f);
        }

        if (sound) {
            hurtSound.play();
        }

        // Set up death animations and states if health is 0
        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            if (sound) {
                dieSound.play();
            }
        }
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;

        // If still alive
        if (health > 0) {
            // State hat controls how long the nemy flashes red when hit
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom3.png");
                    setHeight(128.0f);
                    setWidth(128.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        // Enemy states
        switch (currentState) {
            // Move down the screen
            case MOVING: {
                move(xSpeed * deltaTime, -ySpeed * deltaTime);

                // Stop after a certain point and change states
                if (getPosition().y <= screenHeight * 0.75) {
                    isVulnerable = true;
                    setCurrentState(EnemyState.IDLE);
                }
            }
            break;
            case IDLE: {
                // change state to reload
                lastTime = currentTime;
                setCurrentState(EnemyState.RELOAD);
            }
            break;
            case RELOAD: {
                // If reloading time is over
                if (currentTime - lastTime >= reloadTime) {
                    // Reset the clip capacity
                    clipCapacity = 3;

                    // locate the player
                    target = new Vector2(playerPosition);

                    // fire one shot
                    fire();
                    setCurrentState(EnemyState.FIRE);
                }
            }
            break;
            case FIRE: {
                // Keep firing until clip capacity reaches 0 and if it does set back to IDLE
                if (clipCapacity <= 0)
                    setCurrentState(EnemyState.IDLE);
                if (currentTime - lastTime >= fireRate)
                    fire();
            }
            break;
            // For the enemy is dead
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }

}

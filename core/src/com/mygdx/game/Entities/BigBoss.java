package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class BigBoss extends Enemy {
    // Boss phases
    private enum PHASE {PHASE_1, PHASE_2, PHASE_3}

    // Fire states for certain weapons
    private enum FIRE2_STATE {NONE, ALTERNATE}

    // Move states for phase 3
    private enum MOVE_STATE {STOPPED, CHARGE}

    private MOVE_STATE moveState = MOVE_STATE.STOPPED;

    private FIRE2_STATE fire2State;
    private PHASE bossPhase;

    // Controls minion spawning
    private boolean goonsSpawned = false;
    private Goon goon1;
    public Goon goon2;

    // Different shot speeds and movement speeds
    private long lastTime = 0;
    private float movementSpeed = 400f;
    private float reloadTime = 2000f;
    private float shotSpeed = 450f;
    private float spiralBulletSpeed = 500f;
    private float missileSpeed = 450f;
    private float laserSpeed = 800f;

    // Big Boss weapons 1 fire rate and lifetime
    private float fire1FireRate = 800f;
    private int fire1Cap = 6;

    private float phase1BulletLifetime = 1.5f;


    private float fire2FireRate = 500f;
    private int fire2Cap = 8;

    // Missle controls
    private float missileFireRate = 1200f;
    private int missileDamage = 150;
    private int missileCap = 6;
    private float missileLifetime = 2.5f;

    // laser controls
    private float laserFireRate = 600f;
    private int laserDamage = 25;
    private int laserCap = 7;
    private float laserLifetime = 1.7f;

    // spiral shot controls
    private float spiralFireRate = 150f;
    private int spiralCap = 50;
    private float spiralBulletLifetime = 1.5f;
    private float spiralAngle = 10;

    // direct fire controls
    private float directFireRate = 1500f;
    private int directFireCap = 4;
    private float directFireLifetime = 2f;

    // flame attack controls
    private float flameAttackDuration = 17500f;

    // charge speed controls
    private float chargeSpeed = 800f;
    private float chargeTime = 2000f;
    private boolean isCharging = false;
    private Vector2 chargeTarget;

    // Firing positions and velocities
    private Vector2 firePosition;
    private Vector2 firePosition2;
    private Vector2 rightWeaponVel;
    private Vector2 leftWeaponVel;
    private Vector2 spiralWeaponVel1;
    private Vector2 spiralWeaponVel2;
    private Vector2 target;
    private Vector2 currentTarget;

    // Which weapon the boss will use
    private int weaponNum;
    private boolean flamesSpawned = false;

    private Vector2 bossPosition;

    //Health bar
    private int maxHealth;
    private float healthBar;
    private Texture blank;

    // Same as all the other enemies, initialises textures, sizes, base stats, starting states etc...
    public BigBoss(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.BIG_BOSS;

        setCurrentFrame("bossShip1.png");
        setHeight(250.0f);
        setWidth(350.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);
        blank = new Texture("blank.png");

        bossPosition = new Vector2();
        maxHealth = 9000;
        health = maxHealth;
        defense = 5;
        damageOutput = 10;
        stateTime = 0;

        fire2State = FIRE2_STATE.NONE;
        bossPhase = PHASE.PHASE_1;

        firePosition = new Vector2();
        firePosition2 = new Vector2();
        leftWeaponVel = new Vector2();
        rightWeaponVel = new Vector2();
        target = new Vector2();
        currentTarget = new Vector2();
        spiralWeaponVel1 = new Vector2();
        spiralWeaponVel2 = new Vector2();

        chargeTarget = new Vector2();

        setCurrentState(EnemyState.MOVING);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 8;
        int yOffset = 70;
        int width = (int) getWidth() - 15;
        int height = (int) getHeight() - 120;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    // Gets center of sprite
    public Vector2 getScreenPosition() {
        bossPosition.x = super.position.x;
        bossPosition.y = super.position.y;


        bossPosition.x += width / 2;
        bossPosition.y += height / 2;




        return bossPosition;
    }

    // When ever boss needs to relocate the player
    private void resetCurrentTarget() {
        currentTarget.x = target.x;
        currentTarget.y = target.y;
    }

    // First fire weapon
    public void fire() {
        float rotationDeg = 20;

        // Set up fire positions
        firePosition.x = getPosition().x + getWidth() / 2 - 105;
        firePosition.y = getPosition().y + 120;

        firePosition2.x = firePosition.x + 173;
        firePosition2.y = firePosition.y;

        resetCurrentTarget();

        // set up velocities
        leftWeaponVel.set(currentTarget.sub(firePosition).nor().scl(shotSpeed));

        resetCurrentTarget();

        rightWeaponVel.set(currentTarget.sub(firePosition2).nor().scl(shotSpeed));

        // Spawn bulletes from both weapons
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, phase1BulletLifetime, damageOutput);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition2, 35, 35, rightWeaponVel, phase1BulletLifetime, damageOutput);

        // Spawn bullets slightly angles so it looks like a wave pattern of shots
        for (int i = 0; i < 2; ++i) {
            leftWeaponVel.rotateDeg(rotationDeg);
            rightWeaponVel.rotateDeg(rotationDeg);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, phase1BulletLifetime, damageOutput);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition2, 35, 35, rightWeaponVel, phase1BulletLifetime, damageOutput);
        }

        resetCurrentTarget();


        leftWeaponVel.set(currentTarget.sub(firePosition).nor().scl(shotSpeed));

        resetCurrentTarget();

        rightWeaponVel.set(currentTarget.sub(firePosition2).nor().scl(shotSpeed));

        // Spawn bullets slightly angles so it looks like a wave pattern of shots
        for (int i = 0; i < 2; ++i) {
            leftWeaponVel.rotateDeg(-rotationDeg);
            rightWeaponVel.rotateDeg(-rotationDeg);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, phase1BulletLifetime, damageOutput);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition2, 35, 35, rightWeaponVel, phase1BulletLifetime, damageOutput);
        }

        --fire1Cap;

    }

    // Second fire pattern
    public void fire2() {
        firePosition.x = getPosition().x + getWidth() / 2 - 18;
        firePosition.y = getPosition().y + 120;

        currentTarget.x = 0;
        currentTarget.y = -1;


        leftWeaponVel.set(currentTarget.scl(shotSpeed));

        if (fire2State == FIRE2_STATE.ALTERNATE)
            leftWeaponVel.rotateDeg(-7.5f);

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, phase1BulletLifetime, damageOutput);

        // Fires in a circle
        for (int i = 0; i < 23; ++i) {
            leftWeaponVel.rotateDeg(15);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, phase1BulletLifetime, damageOutput);
        }

        --fire2Cap;
    }

    // Fires missles
    public void fireMissiles() {
        firePosition.x = getPosition().x + getWidth() / 2 - 190;
        firePosition.y = getPosition().y - 50;


        firePosition2.x = firePosition.x + 290;
        firePosition2.y = firePosition.y;

        currentTarget.x = 0;
        currentTarget.y = -1;


        leftWeaponVel.set(currentTarget.scl(missileSpeed));

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.MISSILE, firePosition, 88, 118, leftWeaponVel, missileLifetime, missileDamage);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.MISSILE, firePosition2, 88, 118, leftWeaponVel, missileLifetime, missileDamage);

        --missileCap;
    }

    // Fires lasers
    public void fireLasers() {
        firePosition.x = getPosition().x + getWidth() / 2 - 105;
        firePosition.y = getPosition().y + 120;

        firePosition2.x = firePosition.x + 120;
        firePosition2.y = firePosition.y;

        currentTarget.x = 0;
        currentTarget.y = -1;

        leftWeaponVel = currentTarget.scl(laserSpeed);

        fireLaserFromPosition(firePosition);
        fireLaserFromPosition(firePosition2);

        firePosition.x = getPosition().x + getWidth() / 2 - 155;
        firePosition.y = getPosition().y + 50;

        firePosition2.x = firePosition.x + 225;
        firePosition2.y = firePosition.y;

        fireLaserFromPosition(firePosition);
        fireLaserFromPosition(firePosition2);

        --laserCap;
    }

    // Fires a laser from a certain position on the sprite
    private void fireLaserFromPosition(Vector2 pos) {
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.DOUBLE_BULLET, pos, 75, 200, leftWeaponVel, laserLifetime, laserDamage);
    }

    // Fires in a spiral pattern
    public void fireSpiral() {
        firePosition.x = getPosition().x + getWidth() / 2 - 18;
        firePosition.y = getPosition().y + 120;

        currentTarget.x = -1;
        currentTarget.y = -1;
        leftWeaponVel.set(currentTarget.scl(spiralBulletSpeed));

        currentTarget.x = 1;
        currentTarget.y = 1;
        rightWeaponVel.set(currentTarget.scl(spiralBulletSpeed));

        currentTarget.x = 1;
        currentTarget.y = -1;
        spiralWeaponVel1.set(currentTarget.scl(spiralBulletSpeed));

        currentTarget.x = -1;
        currentTarget.y = 1;
        spiralWeaponVel2.set(currentTarget.scl(spiralBulletSpeed));


        leftWeaponVel.rotateDeg(spiralAngle);
        rightWeaponVel.rotateDeg(spiralAngle);
        spiralWeaponVel1.rotateDeg(spiralAngle);
        spiralWeaponVel2.rotateDeg(spiralAngle);

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, spiralBulletLifetime, 25);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, rightWeaponVel, spiralBulletLifetime, 25);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, spiralWeaponVel1, spiralBulletLifetime, 25);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, spiralWeaponVel2, spiralBulletLifetime, 25);


        spiralAngle += 10;

        if (spiralAngle >= 360)
            spiralAngle = 0;

        --spiralCap;
    }

    // Fires 4 shots at the player
    public void fireAtPlayer() {
        firePosition.x = getPosition().x + getWidth() / 2 - 105;
        firePosition.y = getPosition().y + 120;

        firePosition2.x = firePosition.x + 173;
        firePosition2.y = firePosition.y;

        resetCurrentTarget();
        leftWeaponVel.set(currentTarget.sub(firePosition).nor().scl(shotSpeed));

        resetCurrentTarget();
        rightWeaponVel.set(currentTarget.sub(firePosition2).nor().scl(shotSpeed));

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, leftWeaponVel, directFireLifetime, damageOutput);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition2, 35, 35, rightWeaponVel, directFireLifetime, damageOutput);

        firePosition.x = getPosition().x + getWidth() / 2 - 130;
        firePosition.y = getPosition().y + 50;

        firePosition2.x = firePosition.x + 225;
        firePosition2.y = firePosition.y;

        resetCurrentTarget();
        spiralWeaponVel1.set(currentTarget.sub(firePosition).nor().scl(shotSpeed));

        resetCurrentTarget();
        spiralWeaponVel2.set(currentTarget.sub(firePosition2).nor().scl(shotSpeed));


        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition, 35, 35, spiralWeaponVel1, directFireLifetime, damageOutput);
        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BIG_BOSS_BULLET, firePosition2, 35, 35, spiralWeaponVel2, directFireLifetime, damageOutput);

        --directFireCap;
    }

    // Spawns a ring of flames around the boss
    public void spawnFlames()
    {
        float flameOrbitDegrees = 0;
        float flameLifetime = 1f;

        firePosition.x = getScreenPosition().x - 50;
        firePosition.y = getScreenPosition().y - 80;

        Vector2 velocityFromOrigin = new Vector2(0, 200);



        for(int flameNum = 0; flameNum < 16; ++ flameNum)
        {
            velocityFromOrigin.rotateDeg(flameOrbitDegrees);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.FIRE_ORBIT, firePosition, 100, 100, velocityFromOrigin, flameLifetime, 100);
            flameLifetime += 1;
            flameOrbitDegrees = 22.5f;
        }
    }


    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/bossShip1_DMG.png");
            setHeight(250.0f);
            setWidth(350.0f);
        }

        //hurtSound.play();

        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            //setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            moveState = MOVE_STATE.STOPPED;
            //dieSound.play();
        }
    }

    @Override
    public void dispose() {
    }


    private int getRandomNum(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    // Spawns minions around the boss
    private void spawnGoons() {
        if (!goonsSpawned) {
            goonsSpawned = true;
            goon1 = new Goon(particleSystem, getScreenPosition().x - 66, getScreenPosition().y - 80, 200);
            goon1.setPosition(getScreenPosition().x + 200, getScreenPosition().y);

            goon2 = new Goon(particleSystem, getScreenPosition().x - 66, getScreenPosition().y - 80, -200);
            goon2.reloadTime = 8500f;
            goon2.setPosition(getScreenPosition().x - 200, getScreenPosition().y);


        }
    }

    // Kills the minions
    private void killGoons() {
        if (goonsSpawned) {
            goon1.takeDamage(1000);
            goon2.takeDamage(1000);
            goonsSpawned = false;
        }
    }


    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        stateTime += deltaTime;

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("bossShip1.png");
                    setHeight(250.0f);
                    setWidth(350.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        healthBar = (float) health / maxHealth;

        if (healthBar < 0) {
            healthBar = 0;
        }

        // Change phases based n health
        if (health >= 6000)
            bossPhase = PHASE.PHASE_1;
        else if (health >= 3000) {
            // Phase 2 spawns goons
            bossPhase = PHASE.PHASE_2;
            spawnGoons();
        } else {
            if (goonsSpawned)
                killGoons();

            bossPhase = PHASE.PHASE_3;
        }

        // Update goons if they are spawned
        if (goonsSpawned) {
            goon1.update(deltaTime, playerPosition, currentTime);
            goon2.update(deltaTime, playerPosition, currentTime);

        }

        switch (currentState) {
            // Move down the screen
            case MOVING: {
                move(0, -200 * deltaTime);
                if (getPosition().y <= screenHeight * 0.55) {
                    isVulnerable = true;
                    setCurrentState(EnemyState.IDLE);
                }
            }
            break;
            case IDLE: {
                // Randomly choose a weapon and fire at the player
                lastTime = currentTime;
                weaponNum = getRandomNum(1, 5);
                setCurrentState(EnemyState.FIRE);
            }
            break;
            case RELOAD: {
                // Reload all the different weapon capacities
                if (currentTime - lastTime >= reloadTime) {
                    lastTime = currentTime;
                    target.x = playerPosition.x;
                    target.y = playerPosition.y;

                    fire1Cap = 6;
                    fire2Cap = 8;
                    missileCap = 6;
                    laserCap = 7;
                    spiralCap = 50;
                    directFireCap = 4;

                    setCurrentState(EnemyState.FIRE);

                    weaponNum = getRandomNum(1, 5);

                }
            }
            break;
            // Fire depenign on phase and the chosen weapon for that phase
            case FIRE: {
                if (bossPhase == PHASE.PHASE_1) {
                    if (weaponNum == 1) {
                        if (currentTime - lastTime >= fire1FireRate) {
                            target.x = playerPosition.x;
                            target.y = playerPosition.y;
                            fire();
                            lastTime = currentTime;
                        }
                    } else if(weaponNum == 2)
                    {
                        if (currentTime - lastTime >= fire2FireRate) {
                            if (fire2State == FIRE2_STATE.NONE)
                                fire2State = FIRE2_STATE.ALTERNATE;
                            else
                                fire2State = FIRE2_STATE.NONE;

                            fire2();
                            lastTime = currentTime;
                        }
                    }
                    else if(weaponNum == 3)
                    {
                        if (currentTime - lastTime >= spiralFireRate) {
                            fireSpiral();
                            lastTime = currentTime;
                        }
                    }
                    else
                    {
                        if (currentTime - lastTime >= directFireRate) {
                            target.x = playerPosition.x;
                            target.y = playerPosition.y;
                            fireAtPlayer();
                            lastTime = currentTime;
                        }
                    }
                } else if (bossPhase == PHASE.PHASE_2) {
                    if (weaponNum == 1 || weaponNum == 3) {
                        if (currentTime - lastTime >= missileFireRate) {
                            fireMissiles();
                            lastTime = currentTime;
                        }
                    } else {
                        if (currentTime - lastTime >= laserFireRate) {
                            fireLasers();
                            lastTime = currentTime;
                        }
                    }

                } else if (bossPhase == PHASE.PHASE_3) {
                    if(weaponNum == 1 || weaponNum == 3)
                    {
                        if(!flamesSpawned) {
                            spawnFlames();
                            flamesSpawned = true;
                            lastTime = currentTime;
                        }

                        if(currentTime - lastTime >= flameAttackDuration) {
                            flamesSpawned = false;
                            currentState = EnemyState.RELOAD;
                        }
                    }
                    else
                    {
                        if(!isCharging) {
                            chargeTarget.x = playerPosition.x;
                            chargeTarget.y = playerPosition.y;
                            moveState = MOVE_STATE.CHARGE;
                            leftWeaponVel = chargeTarget.sub(getScreenPosition()).nor().scl(chargeSpeed);
                            lastTime = currentTime;
                            isCharging = true;
                        }
                    }

                }

                if (fire1Cap <= 0 || fire2Cap <= 0 || missileCap <= 0 || laserCap <= 0 || spiralCap <= 0 || directFireCap <= 0)
                    currentState = EnemyState.RELOAD;

            }
            break;
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }

        switch(moveState)
        {
            case CHARGE:
            {
                move(leftWeaponVel.x * deltaTime, leftWeaponVel.y * deltaTime);

                if(currentTime - lastTime >= chargeTime)
                {
                    isCharging = false;
                    currentState = EnemyState.MOVING;
                    setPosition(screenWidth / 2f - (int) width / 2, screenHeight + height*1.5f);
                    moveState = MOVE_STATE.STOPPED;
                }
            }
            break;
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        // Draw boss HP bar
        if (currentState != EnemyState.MOVING || !isDead()){
            batch.setColor(Color.RED);
            batch.draw(blank, position.x + 50, position.y - 20, (width - 100) * healthBar, 5);
            batch.setColor(Color.WHITE);
        }
        if (goonsSpawned) {
            goon1.render(batch);
            goon2.render(batch);
        }
        super.render(batch);
    }
}

package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class LaserEnemy extends Enemy {
    private float ySpeed = 300.0f;
    private float xSpeed = 0;
    private long lastTime = 0;

    private int currentLaserIndex = -1;

    private float aimTime = 300f;
    private float reloadTime = 500f;
    private float liveTime = 2000f;
    private float laserLength = 500f;

    private Vector2 target;
    private Vector2 firePosition;

    public LaserEnemy(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.LASER;

        setCurrentFrame("enemyShipCustom2.png");
        setHeight(128.0f);
        setWidth(128.0f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion2.png", 1, 8, 0.05f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/laserEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        health = 200;
        defense = 1;
        damageOutput = 250;

        stateTime = 0;
        setCurrentState(EnemyState.MOVING);

        firePosition = new Vector2();
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 50;
        int yOffset = 40;
        int width = (int) getWidth() - 100;
        int height = (int) getHeight() - 60;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public void aim() {
        firePosition = new Vector2(getPosition().x + width/2, getPosition().y + 27);
        //firePosition.x = getPosition().x + width/2;
        //firePosition.y = getPosition().y + 27;
        Vector2 tempFirePos = new Vector2(firePosition);
        Vector2 currentTarget = (new Vector2(target)).mulAdd((tempFirePos.sub(target)).nor(), -laserLength);

        //currentTarget.x += 50;
        //currentTarget.y += 50;
        //Gdx.app.log("Laser enemy target x:", Float.toString(currentTarget.x));
        //Gdx.app.log("Laser enemy target y:", Float.toString(currentTarget.y));

        currentLaserIndex = particleSystem.spawnAimLaser(firePosition, currentTarget);
    }

    public void fire() {
        if (currentLaserIndex != -1)
            particleSystem.spawnRedLaser(currentLaserIndex, damageOutput);
        if (sound) fireSound.play();
    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);
        if (sound) hurtSound.play();

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom2_DMG.png");
            setHeight(128.0f);
            setWidth(128.0f);
        }

        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            particleSystem.clearRedLaser(currentLaserIndex);
            currentLaserIndex = -1;

            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            if (sound) dieSound.play();
        }
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom2.png");
                    setHeight(128.0f);
                    setWidth(128.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        switch (currentState) {
            case MOVING: {
                move(xSpeed * deltaTime, -ySpeed * deltaTime);
                if (getPosition().y <= screenHeight * 0.73) {
                    isVulnerable = true;
                    lastTime = currentTime;
                    setCurrentState(EnemyState.IDLE);
                }
            }
            break;
            case IDLE: {
                if (currentTime - lastTime >= aimTime) {
                    target = new Vector2(playerPosition);
                    //Gdx.app.log("Laser enemy target x:", Float.toString(target.x));
                    //Gdx.app.log("Laser enemy target y:", Float.toString(target.y));

                    target.x += 20;
                    target.y += 20;

                    aim();
                    lastTime = currentTime;
                    setCurrentState(EnemyState.RELOAD);
                }
            }
            break;
            case RELOAD: {
                if (currentTime - lastTime >= reloadTime) {
                    fire();
                    lastTime = currentTime;
                    setCurrentState(EnemyState.FIRE);
                }

            }
            break;
            case FIRE: {
                if (currentTime - lastTime >= liveTime) {
                    particleSystem.clearRedLaser(currentLaserIndex);
                    lastTime = currentTime;
                    currentState = EnemyState.IDLE;
                }

            }
            break;
            case EXPLODING: {
                currentAnimation = deathAnimation;


                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            case DEAD: {
                particleSystem.clearRedLaser(currentLaserIndex);
                currentLaserIndex = -1;
            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }
}

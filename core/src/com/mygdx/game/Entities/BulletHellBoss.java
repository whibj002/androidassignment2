package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.mygdx.game.SaveFile;

public class BulletHellBoss extends Enemy {

    private float h = Gdx.graphics.getHeight();
    private float w = Gdx.graphics.getWidth();

    // Movement variables
    private float ySpeed = 300.0f;
    private float xSpeed = 35.0f;

    // Bullet variables
    private float reloadTime = 3000f;
    private float fireRate = 150f;
    private float shotSpeed = 400f;
    private int damage = 15;
    private int explodey_damage = 25;
    private int clipCapacity = 6;
    private long lastTime = 0;

    private Vector2 target;

    private Sound fireSound;
    private Sound dieSound;
    private Sound hurtSound;

    // Strafe direction
    public enum Direction {LEFT, RIGHT}
    public Direction direction;

    // Shot pattern
    public enum ShotPattern {STAGE_ONE, STAGE_TWO, STAGE_THREE}
    ShotPattern shotPattern;
    private boolean alterPattern;

    //Health bar
    private int maxHealth;
    private float healthBar;
    private Texture blank;

    public BulletHellBoss(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.BULLET_HELL_BOSS;

        setCurrentFrame("bossShip2.png");
        setHeight(h / 2.4f);
        setWidth(w / 1.2f);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);
        blank = new Texture("blank.png");

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/standardEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        maxHealth = 5000;
        health = maxHealth;
        defense = 6;
        stateTime = 0;

        setCurrentState(EnemyState.MOVING);
        direction = Direction.LEFT;
        shotPattern = ShotPattern.STAGE_ONE;
        alterPattern = true;
    }

    @Override
    public void dispose() {
        super.dispose();
        blank.dispose();
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 50;
        int yOffset = 120;
        int width = (int) getWidth() - 100;
        int height = (int) getHeight() - 150;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    /**
     * Uses Vector2's for fireposition and projectile velocity.
     * Spawns particles based on fireposition and velocity vector value. Generally changes the angle and speed of projectile.
     */
    public void fire() {
        Vector2 fp;
        Vector2 v;
        // Bullet rain pattern
        if (shotPattern == ShotPattern.STAGE_ONE) {
            fireRate = 150f;
            // alter pattern after each shot
            if (alterPattern){
                fp = new Vector2(getPosition().x + getWidth() / 2 - 240, h);
                v = (new Vector2(0, -1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2 - 120, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2 + 120, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2 + 240, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                alterPattern = false;
            }
            else if (!alterPattern){
                fp = new Vector2(getPosition().x + getWidth() / 2 - 180, h);
                v = (new Vector2(0, -1)).scl(shotSpeed);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2 - 60, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2 + 60, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                fp = new Vector2(getPosition().x + getWidth() / 2 + 180, h);
                particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, fp, 50, 50, v, 3.0f, damage);
                alterPattern = true;
            }

        }
        // Explodey pattern
        else if (shotPattern == ShotPattern.STAGE_TWO) {
            fireRate = 400f;
            Vector2 firePosition = new Vector2(getPosition().x + getWidth() / 2 - 225, getPosition().y);
            Vector2 firePosition2 = new Vector2(getPosition().x + getWidth() / 2 - 200, getPosition().y);

            Vector2 currentTarget = new Vector2(-125, 0);
            Vector2 currentTarget2 = new Vector2(125, 0);
            Vector2 velocity = (currentTarget.sub(firePosition).nor().scl(shotSpeed / 2f));
            Vector2 velocity2 = (currentTarget2.sub(firePosition).nor().scl(shotSpeed / 4f));

            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET2, firePosition, 350, 200, velocity, 1.25f, explodey_damage);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET2, firePosition2, 350, 200, velocity2, 2.5f, explodey_damage);

        }
        // Bullet1 + Boss bullet wave
        else if (shotPattern == ShotPattern.STAGE_THREE) {
            fireRate = 400f;
            Vector2 firePosition = new Vector2(getPosition().x + getWidth() / 2 - 22, getPosition().y);
            Vector2 firePosition2 = new Vector2(getPosition().x + getWidth() / 2 - 88, getPosition().y);
            Vector2 firePosition3 = new Vector2(getPosition().x + getWidth() / 2 - 154, getPosition().y);
            Vector2 firePosition4 = new Vector2(getPosition().x + getWidth() / 2 + 22, getPosition().y);
            Vector2 firePosition5 = new Vector2(getPosition().x + getWidth() / 2 + 88, getPosition().y);
            Vector2 firePosition6 = new Vector2(getPosition().x + getWidth() / 2 + 154, getPosition().y);
            Vector2 currentTarget = new Vector2(target);
            Vector2 currentTargetLeft = new Vector2(target.x - 50, target.y);
            Vector2 currentTargetLeft2 = new Vector2(target.x - 100, target.y);
            Vector2 currentTargetRight = new Vector2(target.x + 50, target.y);
            Vector2 currentTargetRight2 = new Vector2(target.x + 100, target.y);
            Vector2 currentTargetRight3 = new Vector2(target.x + 150, target.y);

            Vector2 velocity = (currentTarget.sub(firePosition).nor().scl(shotSpeed));
            Vector2 velocity2 = (currentTargetLeft.sub(firePosition).nor().scl(shotSpeed));
            Vector2 velocity3 = (currentTargetLeft2.sub(firePosition).nor().scl(shotSpeed));
            Vector2 velocity4 = (currentTargetRight.sub(firePosition).nor().scl(shotSpeed));
            Vector2 velocity5 = (currentTargetRight2.sub(firePosition).nor().scl(shotSpeed));
            Vector2 velocity6 = (currentTargetRight3.sub(firePosition).nor().scl(shotSpeed));

            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, firePosition, 50, 50, velocity, 3.0f, damage);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.HELL_BOSS_BULLET, firePosition2, 50, 50, velocity2, 3.0f, damage);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, firePosition3, 50, 50, velocity3, 3.0f, damage);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.HELL_BOSS_BULLET, firePosition4, 50, 50, velocity4, 3.0f, damage);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.BULLET1, firePosition5, 50, 50, velocity5, 3.0f, damage);
            particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.HELL_BOSS_BULLET, firePosition6, 50, 50, velocity6, 3.0f, damage);
        }

        lastTime = System.currentTimeMillis();
        --clipCapacity;
        if (sound) fireSound.play();
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        stateTime += deltaTime;

        if (health > 0) {
            // Handle flashing on hit
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("bossShip2.png");
                    setHeight(h / 2.4f);
                    setWidth(w / 1.2f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        // Update health bar
        healthBar = (float) health / maxHealth;
        if (healthBar < 0) {
            healthBar = 0;
        }

        // Handle boss states
        switch (currentState) {
            // Moving = Boss moving into position. Begin moving boss down, set to invulnerable.
            case MOVING: {
                move(0, -ySpeed * deltaTime);
                if (getPosition().y <= screenHeight * 0.60) {
                    isVulnerable = true;
                    setCurrentState(EnemyState.IDLE);
                }
            }
            break;
            // Idle = Boss has moved into position. Begin strafing.
            case IDLE: {
                strafe(deltaTime);
                lastTime = currentTime;
                setCurrentState(EnemyState.RELOAD);
            }
            break;
            // Continue strafing while reloading
            case RELOAD: {
                strafe(deltaTime);

                if (currentTime - lastTime >= reloadTime) {

                    clipCapacity = 6;
                    target = new Vector2(playerPosition);
                    fire();
                    setCurrentState(EnemyState.FIRE);
                }
            }
            break;
            // Strafe boss, decide shotpattern and fire.
            case FIRE: {
                strafe(deltaTime);

                if (clipCapacity <= 0) {
                    setCurrentState(EnemyState.IDLE);
                    // Cycle through patterns
                    if (shotPattern == ShotPattern.STAGE_ONE){
                        shotPattern = ShotPattern.STAGE_TWO;
                    }
                    else if (shotPattern == ShotPattern.STAGE_TWO)
                        shotPattern = ShotPattern.STAGE_THREE;
                    else if (shotPattern == ShotPattern.STAGE_THREE)
                        shotPattern = ShotPattern.STAGE_ONE;
                }
                if (currentTime - lastTime >= fireRate) {
                    fire();
                    if (sound) fireSound.play(0.5f);
                }
            }
            break;
            // Exploding - explode animation, change state to dead
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;
            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        // Draw boss HP bar
        if (!isDead()) {
            batch.setColor(Color.RED);
            batch.draw(blank, position.x + 50, position.y - 20, (width - 100) * healthBar, 5);
            batch.setColor(Color.WHITE);
        }
        if (currentAnimation != null) {
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));
        }
        super.render(batch);
    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        // If hp > =, flash animation
        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/bossShip2_DMG.png");
            setHeight(h / 2.4f);
            setWidth(w / 1.2f);
        }

        if (sound) hurtSound.play();
        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            if (sound) dieSound.play();
        }
    }

    /**
     * Strafe left to right and within screen bounds
     * @param deltaTime to dictate accurate speed of translation
     */
    public void strafe(float deltaTime) {
        if (direction == Direction.LEFT) {
            if (getPosition().x <= 0) {
                direction = Direction.RIGHT;
            }
            move(-xSpeed * deltaTime, 0);
        } else if (direction == Direction.RIGHT) {
            if (getPosition().x >= screenWidth - getWidth()) {
                direction = Direction.LEFT;
            }
            move(xSpeed * deltaTime, 0);
        }
    }
}

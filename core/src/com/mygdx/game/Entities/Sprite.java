package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Sprite {
    protected TextureRegion currentFrame;

    protected Vector2 position;
    protected Vector2 origin;
    protected Vector2 scale;
    protected float rotation;

    protected float width;
    protected float height;

    private Rectangle boundingBox;

    public Sprite() {
        position = new Vector2();
        origin = new Vector2();
        scale = new Vector2();
        rotation = 0;
        position.x = 0;
        position.y = 0;
        origin.x = 0;
        origin.y = 0;
        scale.x = 0;
        scale.y = 0;
        width = 0;
        height = 0;
        currentFrame = new TextureRegion();
        boundingBox = new Rectangle(position.x, position.y, width, height);
    }

    public void dispose() {
        if (currentFrame != null) {
            currentFrame.getTexture().dispose();
        }
    }

    public void setPosition(float x, float y) {
        position.x = x;
        position.y = y;
    }

    public Vector2 getOrigin() {
        return origin;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setWidth(float _width) {
        width = _width;
    }

    public void setHeight(float _height) {
        height = _height;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setCurrentFrame(Texture texture) {
        currentFrame.setRegion(texture);
    }

    public void setCurrentFrame(TextureRegion texture) {
        currentFrame.setRegion(texture);
    }


    public void setCurrentFrame(String path) {
        Texture temp = new Texture(Gdx.files.internal(path));
        currentFrame.setRegion(temp);
        width = currentFrame.getRegionWidth();
        height = currentFrame.getRegionHeight();
    }

    public TextureRegion getCurrentFrame() {
        return currentFrame;
    }

    protected Rectangle getBoundingBox(int xOffset, int yOffset, int width, int height) {
        boundingBox.setWidth(width);
        boundingBox.setHeight(height);
        boundingBox.setPosition(position.x + xOffset, position.y + yOffset);
        return boundingBox;
    }

    public void move(float x, float y) {
        position.x += x;
        position.y += y;
    }

    /**
     * Renders the sprite onto the screen
     * @param batch
     */
    public void render(SpriteBatch batch) {
        if (currentFrame != null)
            batch.draw(currentFrame, position.x, position.y, width, height);

    }

    /**
     * Renders sprites that rotate
     * @param batch
     */
    public void render2(SpriteBatch batch) {
        if (currentFrame != null)
            batch.draw(currentFrame, position.x, position.y, origin.x, origin.y, width, height, scale.x, scale.y, rotation);
    }

    public static Animation<TextureRegion> loadAnimation(String path, int rows, int cols, float frameDuration) {
        // Get the animation sheet
        Texture animationSheet = new Texture(Gdx.files.internal(path));

        // Divide it up
        TextureRegion[][] temp = TextureRegion.split(animationSheet, animationSheet.getWidth() / cols, animationSheet.getHeight() / rows);

        TextureRegion[] animationFrames = new TextureRegion[rows * cols];

        // Put contents of 2D array into normal array
        int index = 0;
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                animationFrames[index++] = temp[i][j];
            }
        }
        // Make and return the animation
        return new Animation<>(frameDuration, animationFrames);
    }
}

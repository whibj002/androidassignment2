package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class ParticleSystem {
    // Max particles allowed
    int maxParticles = 200;
    int maxBlackHoles = 50;
    int latestBlackHoleIndex = 0;

    // Tells particle system how many enemies in a round
    public int maxEnemies = 0;

    // All the different particle types
    public enum PARTICLE_TYPE {NONE, BULLET1, BULLET2, DOUBLE_BULLET, HOLE_BULLET, BLACK_HOLE, ION, PELLET, LASER_AIM,
        LASER_RED, PLAYER_BULLET1, HELL_BOSS_BULLET, BIG_BOSS_BULLET, MISSILE, FIRE_ORBIT, FIRE_PROJECTILE}

    TextureRegion bullet1;
    TextureRegion playerBullet1;
    TextureRegion bullet2;
    TextureRegion pellet;
    TextureRegion ionBullet;
    TextureRegion holeBullet;
    TextureRegion blackHole;
    TextureRegion doubleBullet;
    TextureRegion bossBullet1;
    TextureRegion missile;
    TextureRegion fire;

    Animation fireAnimation;

    public Vector2 playerBulletPosition;

    // Everything that a particle needs
    public PARTICLE_TYPE[] particleTypes = new PARTICLE_TYPE[maxParticles];
    public Vector2[] position = new Vector2[maxParticles];
    public float[] width = new float[maxParticles];
    public float[] height = new float[maxParticles];
    public Vector2[] velocity = new Vector2[maxParticles];
    public float[] lifetime = new float[maxParticles];
    public int[] bulletDamage = new int[maxParticles];
    // Special array for blackhole indexes since player bullet interacts with them
    public int[] blackHoleIndexes = new int[maxBlackHoles];

    // Rotation arrays for rendering and rotation calculations
    public float[] missileRotation = new float[maxParticles];
    private Vector2[] currentMissileRotation = new Vector2[maxParticles];

    // for animations
    public float[] stateTime = new float[maxParticles];

    //public Vector2[] bulletTarget = new Vector2[maxParticles];

    public Vector2[] directionVectors = new Vector2[8];

    public Enemy[] enemies;
    public int enemiesThisRound;

    // All the different hitboxes for particles
    public Rectangle bullet1Box;
    public Rectangle bullet2Box;
    public Rectangle playerBulletBox;
    public Polygon polyBox;
    public Polygon missileBox;
    float[] polyVertices;
    public Circle blackHoleCircle;
    public Circle suctionCircle;
    public Vector2 circleCenters;
    Vector2 bulletPosition;
    public float damageRadius = 30f;
    public float suctionRadius = 120f;

    Vector2 flameBoxPos = new Vector2();


    private Vector2 currentMissileVector;

    // Speeds for particles spawned by the particle systemclass
    private float pelletSpeed = 500f;
    private float blackHoleSpeed = 130f;
    private Player player;

    public ParticleSystem() {
    }

    // Set up textures, initialise arrays etc..
    public void setUp() {
        bullet1 = new TextureRegion(new Texture(Gdx.files.internal("bullet1.png")));
        pellet = new TextureRegion(new Texture(Gdx.files.internal("Pellet.png")));
        ionBullet = new TextureRegion(new Texture(Gdx.files.internal("ionBullet.png")));
        doubleBullet = new TextureRegion(new Texture(Gdx.files.internal("doubleBullet.png")));
        bossBullet1 = new TextureRegion(new Texture(Gdx.files.internal("bossBullet1.png")));
        bullet1Box = new Rectangle();

        enemies = new Enemy[2];

        bullet2 = new TextureRegion(new Texture(Gdx.files.internal("bullet2.png")));
        bullet2Box = new Rectangle();

        playerBullet1 = new TextureRegion(new Texture(Gdx.files.internal("playerBullet1.png")));
        playerBulletBox = new Rectangle();
        playerBulletPosition = new Vector2();

        holeBullet = new TextureRegion(new Texture(Gdx.files.internal("HoleBullet.png")));
        blackHole = new TextureRegion(new Texture(Gdx.files.internal("BlackHole.png")));
        blackHoleCircle = new Circle();
        suctionCircle = new Circle();
        circleCenters = new Vector2();
        bulletPosition = new Vector2();

        polyBox = new Polygon(new float[]{0, 0, 0, 0, 0, 0, 0, 0});
        polyVertices = new float[8];

        missileBox = new Polygon(new float[]{0, 0, 0, 0, 0, 0});

        missile = new TextureRegion(new Texture(Gdx.files.internal("missile.png")));

        currentMissileVector = new Vector2();

        fireAnimation = Sprite.loadAnimation("fireAnimation.png", 6, 10, 0.033f);
        fire = (TextureRegion)fireAnimation.getKeyFrames()[0];

        for (int i = 0; i < maxParticles; ++i) {
            particleTypes[i] = PARTICLE_TYPE.NONE;
            position[i] = new Vector2(0, 0);
            width[i] = 0;
            height[i] = 0;
            velocity[i] = new Vector2(0, 0);
            lifetime[i] = 0;
            bulletDamage[i] = 0;
            missileRotation[i] = 0;
            currentMissileRotation[i] = new Vector2(0, 0);
            //bulletTarget[i] = new Vector2(0,0);
            stateTime[i] = 0;

        }

        directionVectors[0] = new Vector2(0, 1).scl(pelletSpeed);
        directionVectors[1] = new Vector2(1, 1).scl(pelletSpeed);
        directionVectors[2] = new Vector2(1, 0).scl(pelletSpeed);
        directionVectors[3] = new Vector2(1, -1).scl(pelletSpeed);
        directionVectors[4] = new Vector2(0, -1).scl(pelletSpeed);
        directionVectors[5] = new Vector2(-1, -1).scl(pelletSpeed);
        directionVectors[6] = new Vector2(-1, 0).scl(pelletSpeed);
        directionVectors[7] = new Vector2(-1, 1).scl(pelletSpeed);
    }

    public void setPlayer(Player p) {
        player = p;
    }

    public Player getPlayer() {
        return player;
    }

    // Add a particle to the array
    public void spawn(PARTICLE_TYPE type, Vector2 pos, float w, float h, Vector2 vel, float lifeTime, int damage) {
        if (type != PARTICLE_TYPE.NONE) {
            for (int index = 0; index < maxParticles; ++index) {
                if (particleTypes[index] == PARTICLE_TYPE.NONE) {
                    particleTypes[index] = type;
                    position[index].x = pos.x;
                    position[index].y = pos.y;
                    width[index] = w;
                    height[index] = h;
                    velocity[index].x = vel.x;
                    velocity[index].y = vel.y;
                    lifetime[index] = lifeTime;
                    bulletDamage[index] = damage;

                    // Keep track of black holes and fires
                    if (particleTypes[index] == PARTICLE_TYPE.BLACK_HOLE) {

                        blackHoleIndexes[latestBlackHoleIndex] = index;
                        ++latestBlackHoleIndex;
                    }
                    else if (particleTypes[index] == PARTICLE_TYPE.FIRE_ORBIT)
                    {
                        stateTime[index] = 0;
                        missileRotation[index] = 0;
                        currentMissileRotation[index].x = pos.x;
                        currentMissileRotation[index].y = pos.y;
                    }
                    break;
                }
            }
        }
    }

    // Set a polygon as a collison object for particles that rotate
    private void setPolyBox(Rectangle r, Vector2 pos) {
        //polyBox = new Polygon();
        polyVertices[0] = pos.x;
        polyVertices[1] = pos.y;
        polyVertices[2] = pos.x + r.getWidth();
        polyVertices[3] = pos.y;
        polyVertices[4] = pos.x + r.getWidth();
        polyVertices[5] = pos.y + r.getHeight();
        polyVertices[6] = pos.x;
        polyVertices[7] = pos.y + r.getHeight();

        polyBox.setVertices(polyVertices);
        //polyBox.setOrigin(r.getWidth()/2, r.getHeight()/2);
    }

    // For spawning lasers
    public int spawnAimLaser(Vector2 pos, Vector2 direction) {
        for (int index = 0; index < maxParticles; ++index) {
            if (particleTypes[index] == PARTICLE_TYPE.NONE) {
                particleTypes[index] = PARTICLE_TYPE.LASER_AIM;
                position[index] = pos;
                velocity[index] = new Vector2(direction);
                lifetime[index] = 1;
                return index;
            }
        }

        return -1;
    }

    // more laser spawning
    public void spawnRedLaser(int index, int damagePerSec) {
        if (index >= 0 && index < maxParticles) {
            particleTypes[index] = PARTICLE_TYPE.LASER_RED;
            bulletDamage[index] = damagePerSec;
        }
    }

    // Get rid of lasers on screen
    public void clearRedLaser(int index) {
        if (index >= 0 && index < maxParticles) {
            if (particleTypes[index] != PARTICLE_TYPE.NONE)
                particleTypes[index] = PARTICLE_TYPE.NONE;
        }

    }

    // Set the hitbox of particles depending on there position, particles can have different types of hitboxes, some particles need circles
    // and some need polygons
    public void setHitBox(PARTICLE_TYPE type, Vector2 bulletPos, float w, float h) {
        float xOffset = 0;
        float yOffset = 0;
        float bullWidth = w;
        float bullHeight = h;

        if (type != PARTICLE_TYPE.NONE) {
            if (type == PARTICLE_TYPE.BULLET1) {
                xOffset = 20;
                yOffset = 21;
                bullWidth = w - 40;
                bullHeight = h - 40;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.PLAYER_BULLET1) {
                xOffset = 20;
                yOffset = 21;
                bullWidth = w - 40;
                bullHeight = h - 40;

                playerBulletBox.setWidth(bullWidth);
                playerBulletBox.setHeight(bullHeight);
                playerBulletBox.setX(bulletPos.x + xOffset);
                playerBulletBox.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.BULLET2) {
                xOffset = 156;
                yOffset = 85;
                bullWidth = w - 315;
                bullHeight = h - 165;

                bullet2Box.setWidth(bullWidth);
                bullet2Box.setHeight(bullHeight);
                bullet2Box.setX(bulletPos.x + xOffset);
                bullet2Box.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.PELLET) {
                xOffset = 40;
                yOffset = 45;
                bullWidth = w - 80;
                bullHeight = h - 90;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.ION) {
                xOffset = 27;
                yOffset = 30;
                bullWidth = w - 55;
                bullHeight = h - 60;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.HOLE_BULLET) {
                xOffset = 40;
                yOffset = 39;
                bullWidth = w - 80;
                bullHeight = h - 80;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.BLACK_HOLE) {
                xOffset = 75;
                yOffset = 63;

                blackHoleCircle.setX(bulletPos.x + xOffset);
                blackHoleCircle.setY(bulletPos.y + yOffset);

                suctionCircle.setX(bulletPos.x + xOffset);
                suctionCircle.setY(bulletPos.y + yOffset);
                blackHoleCircle.setRadius(damageRadius);
                suctionCircle.setRadius(suctionRadius);
            } else if (type == PARTICLE_TYPE.DOUBLE_BULLET) {
                xOffset = 29.5f;
                yOffset = 50;
                bullWidth = w - 60;
                bullHeight = h - 70;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);
            }
            else if (type == PARTICLE_TYPE.HELL_BOSS_BULLET) {
                xOffset = 27;
                yOffset = 30;
                bullWidth = w - 80;
                bullHeight = h - 90;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);

            } else if (type == PARTICLE_TYPE.BIG_BOSS_BULLET) {
                xOffset = 8;
                yOffset = 7;
                bullWidth = 20;
                bullHeight = 20;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);
            } else if (type == PARTICLE_TYPE.MISSILE) {
                xOffset = bulletPos.x + 45.5f;
                yOffset = bulletPos.y + 25;

                missileBox.setVertices(new float[]{xOffset - 1, yOffset, xOffset - 10, yOffset + 50, xOffset + 10, yOffset + 50});
                missileBox.setOrigin(xOffset, yOffset + 30);
            }
            else if (type == PARTICLE_TYPE.FIRE_ORBIT || type == PARTICLE_TYPE.FIRE_PROJECTILE)
            {
                xOffset = 43;
                yOffset = 10;
                bullWidth = w - 80;
                bullHeight = h - 40;

                bullet1Box.setWidth(bullWidth);
                bullet1Box.setHeight(bullHeight);
                bullet1Box.setX(bulletPos.x + xOffset);
                bullet1Box.setY(bulletPos.y + yOffset);


                bullet1Box.getPosition(flameBoxPos);

                setPolyBox(bullet1Box, flameBoxPos);
                polyBox.setOrigin(bulletPos.x + xOffset + bullWidth/2 - 8, bulletPos.y + yOffset + bullHeight/2 + 5);
            }
        }
    }

    // Updates every particle on screen
    public void update(float deltaTime, float playerX, float playerY) {
        for (int index = 0; index < maxParticles; ++index) {
            if (particleTypes[index] != PARTICLE_TYPE.NONE) {

                if (particleTypes[index] != PARTICLE_TYPE.LASER_AIM)
                    lifetime[index] -= deltaTime;

                // Particle Movement, some particles don't move the same as hours so that needs to be accoutned for
                if (lifetime[index] > 0) {
                    if (particleTypes[index] == PARTICLE_TYPE.BULLET1 || particleTypes[index] == PARTICLE_TYPE.PLAYER_BULLET1
                            || particleTypes[index] == PARTICLE_TYPE.BULLET2 || particleTypes[index] == PARTICLE_TYPE.PELLET
                            || particleTypes[index] == PARTICLE_TYPE.ION || particleTypes[index] == PARTICLE_TYPE.HOLE_BULLET
                            || particleTypes[index] == PARTICLE_TYPE.BLACK_HOLE || particleTypes[index] == PARTICLE_TYPE.DOUBLE_BULLET
                            || particleTypes[index] == PARTICLE_TYPE.HELL_BOSS_BULLET || particleTypes[index] == PARTICLE_TYPE.BIG_BOSS_BULLET
                            || particleTypes[index] == PARTICLE_TYPE.FIRE_PROJECTILE)
                    {
                        position[index].mulAdd(velocity[index], deltaTime);
                    }
                    else if (particleTypes[index] == PARTICLE_TYPE.MISSILE) {
                        //missileBox.setOrigin(width[index]/2, height[index]/2);
                        if (lifetime[index] > 1.75) {
                            currentMissileRotation[index].x = playerX - position[index].x;
                            currentMissileRotation[index].y = playerY - position[index].y;

                            missileRotation[index] = currentMissileRotation[index].angleDeg() + 90;

                            currentMissileVector.x = currentMissileRotation[index].x;
                            currentMissileVector.y = currentMissileRotation[index].y;

                            currentMissileVector = currentMissileVector.nor().scl(450f);
                            position[index].mulAdd(currentMissileVector, deltaTime);
                        } else {
                            //missileRotation[index] = currentMissileRotation.angleDeg() + 90;
                            currentMissileRotation[index] = currentMissileRotation[index].nor().scl(450f);
                            position[index].mulAdd(currentMissileRotation[index], deltaTime);
                        }
                    }
                    else if (particleTypes[index] == PARTICLE_TYPE.FIRE_ORBIT)
                    {
                        stateTime[index] += deltaTime;

                        position[index].set(currentMissileRotation[index]);
                        velocity[index].rotateDeg(70 * deltaTime);
                        position[index].add(velocity[index]);
                    }

                }
                else
                {
                    if (particleTypes[index] == PARTICLE_TYPE.BULLET2) {
                        for (int i = 0; i < 8; ++i) {
                            Vector2 pelPos = new Vector2(position[index].x + 125, position[index].y + 50);
                            this.spawn(PARTICLE_TYPE.PELLET, pelPos, 100, 100, directionVectors[i], 2.0f, bulletDamage[index] / 2);
                        }

                    } else if (particleTypes[index] == PARTICLE_TYPE.HOLE_BULLET) {
                        // CHANGE THIS TO NOT USE "new" OPERATOR
                        Vector2 pelPos = new Vector2(position[index].x - 25, position[index].y);
                        this.spawn(PARTICLE_TYPE.BLACK_HOLE, pelPos, 150, 125, new Vector2(0, -1).scl(blackHoleSpeed), 4f, 1000);
                    } else if (particleTypes[index] == PARTICLE_TYPE.BLACK_HOLE) {
                        --latestBlackHoleIndex;
                    }
                    else if (particleTypes[index] == PARTICLE_TYPE.FIRE_ORBIT)
                    {
                        particleTypes[index] = PARTICLE_TYPE.FIRE_PROJECTILE;
                        lifetime[index] = 2f;

                        currentMissileRotation[index].x = playerX - position[index].x;
                        currentMissileRotation[index].y = playerY - position[index].y;

                        missileRotation[index] = currentMissileRotation[index].angleDeg() + 90;

                        velocity[index].x = currentMissileRotation[index].x;
                        velocity[index].y = currentMissileRotation[index].y;

                        velocity[index] = velocity[index].nor().scl(700f);
                        continue;
                    }
                    particleTypes[index] = PARTICLE_TYPE.NONE;
                }



                // Collisions
                if (lifetime[index] > 0) {
                    float distance = position[index].dst2(playerX, playerY);

                    if (distance < 0)
                        distance *= -1;
                    // Boss bullet
                    if (particleTypes[index] == PARTICLE_TYPE.HELL_BOSS_BULLET) {
                        if (player.getBoundingBox().overlaps(bullet1Box)) {
                            player.takeDamage(bulletDamage[index]);
                            bullet1Box.setPosition(800, 800);
                            particleTypes[index] = PARTICLE_TYPE.NONE;
                        }
                    }
                    // Check if bullet is close to player
                    if (distance <= 30000 && particleTypes[index] != PARTICLE_TYPE.PLAYER_BULLET1 && particleTypes[index] != PARTICLE_TYPE.NONE
                            && particleTypes[index] != PARTICLE_TYPE.LASER_AIM && particleTypes[index] != PARTICLE_TYPE.LASER_RED) {

                        // Set a hitbox to to the particles that are close to player on screen
                        setHitBox(particleTypes[index], position[index], width[index], height[index]);

                        // Check if they collide witht he player and damage the player
                        if (particleTypes[index] == PARTICLE_TYPE.BULLET1 || particleTypes[index] == PARTICLE_TYPE.PELLET
                                || particleTypes[index] == PARTICLE_TYPE.ION || particleTypes[index] == PARTICLE_TYPE.HOLE_BULLET
                                || particleTypes[index] == PARTICLE_TYPE.DOUBLE_BULLET || particleTypes[index] == PARTICLE_TYPE.BIG_BOSS_BULLET
                                || particleTypes[index] == PARTICLE_TYPE.HELL_BOSS_BULLET) {
                            if (player.getBoundingBox().overlaps(bullet1Box)) {
                                if (particleTypes[index] == PARTICLE_TYPE.ION) {
                                    player.disableFire();
                                } else {
                                    player.takeDamage(bulletDamage[index]);
                                    bullet1Box.setPosition(800, 800);

                                }
                                particleTypes[index] = PARTICLE_TYPE.NONE;
                            }

                        } else if (particleTypes[index] == PARTICLE_TYPE.BLACK_HOLE) {
                            Rectangle r = player.getBoundingBox();
                            if (Intersector.overlaps(suctionCircle, r)) {

                                circleCenters.x = suctionCircle.x;
                                circleCenters.y = suctionCircle.y;

                                bulletPosition.x = player.getScreenPosition().x;
                                bulletPosition.y = player.getScreenPosition().y;

                                Vector2 newVelocity = circleCenters.sub(bulletPosition).nor().scl(800f);

                                //player.move(newVelocity.x * deltaTime, newVelocity.y * deltaTime);
                                player.playerDelta.lerp(newVelocity, 0.2f);
                                player.move(player.playerDelta.x * deltaTime, player.playerDelta.y * deltaTime);

                            }

                            if (Intersector.overlaps(blackHoleCircle, r)) {
                                player.takeDamage(bulletDamage[index]);
                            }
                        }
                        else if (particleTypes[index] == PARTICLE_TYPE.MISSILE)
                        {
                            missileBox.setRotation(currentMissileRotation[index].angleDeg() + 90);

                            if (Intersector.overlapConvexPolygons(missileBox, player.getBoundingPoly())) {
                                particleTypes[index] = PARTICLE_TYPE.NONE;
                                player.takeDamage(bulletDamage[index]);

                            }
                        }
                        else if (particleTypes[index] == PARTICLE_TYPE.BULLET2)
                        {
                            if (player.getBoundingBox().overlaps(bullet2Box)) {
                                player.takeDamage(bulletDamage[index]);
                                bullet2Box.setPosition(800, 800);
                                particleTypes[index] = PARTICLE_TYPE.NONE;
                            }
                        }
                        else if (particleTypes[index] == PARTICLE_TYPE.FIRE_PROJECTILE || particleTypes[index] == PARTICLE_TYPE.FIRE_ORBIT)
                        {

                            polyBox.setRotation(missileRotation[index]);

                            if(Intersector.overlapConvexPolygons(polyBox, player.getBoundingPoly()))
                            {
                                player.takeDamage(bulletDamage[index]);
                                particleTypes[index] = PARTICLE_TYPE.NONE;
                            }
                        }


                    } else if (particleTypes[index] == PARTICLE_TYPE.PLAYER_BULLET1) {
                        //Check if player bullets collide with enemies
                        for (int i = 0; i < enemiesThisRound; ++i) {

                            if (enemies[i] != null) {
                                //Gdx.app.log("Detected enemy:", "True");
                                if (enemies[i].getPosition().dst2(position[index]) <= 62500) {
                                    //Gdx.app.log("Bullets in range:", "true");
                                    setHitBox(PARTICLE_TYPE.PLAYER_BULLET1, position[index], width[index], height[index]);

                                    if (enemies[i].enemyType == Enemy.EnemyType.ION) {
                                        //Gdx.app.log("Detected Ion Enemy", "true");
                                        playerBulletPosition.x = playerBulletBox.x;
                                        playerBulletPosition.y = playerBulletBox.y;
                                        setPolyBox(playerBulletBox, playerBulletPosition);

                                        if (Intersector.overlapConvexPolygons(enemies[i].getBoundingPoly(), polyBox)) {
                                            enemies[i].takeDamage(bulletDamage[index]);
                                            particleTypes[index] = PARTICLE_TYPE.NONE;
                                        }
                                    } else if (enemies[i].getBoundingBox().overlaps(playerBulletBox) || enemies[i].getBoundingBox().contains(playerBulletBox)) {
                                        //Gdx.app.log("Hit Enemy:", "True");
                                        enemies[i].takeDamage(bulletDamage[index]);
                                        playerBulletBox.setPosition(900, 900);
                                        particleTypes[index] = PARTICLE_TYPE.NONE;
                                        break;
                                    }
                                }
                            }
                        }
                        // Check if player bullters are near a black hole
                        for (int i = 0; i < maxBlackHoles; ++i) {
                            if (particleTypes[blackHoleIndexes[i]] != PARTICLE_TYPE.NONE && particleTypes[index] != PARTICLE_TYPE.NONE) {
                                if (particleTypes[blackHoleIndexes[i]] == PARTICLE_TYPE.BLACK_HOLE) {
                                    setHitBox(PARTICLE_TYPE.BLACK_HOLE, position[blackHoleIndexes[i]], width[blackHoleIndexes[i]], height[blackHoleIndexes[i]]);
                                    setHitBox(PARTICLE_TYPE.PLAYER_BULLET1, position[index], width[index], height[index]);

                                    bulletPosition.x = playerBulletBox.x;
                                    bulletPosition.y = playerBulletBox.y;

                                    if (bulletPosition.dst2(new Vector2(blackHoleCircle.x, blackHoleCircle.y)) <= 40000) {
                                        if (Intersector.overlaps(suctionCircle, playerBulletBox)) {
                                            circleCenters.x = suctionCircle.x;
                                            circleCenters.y = suctionCircle.y;

                                            //Vector2 directionVector = new Vector2(velocity[index]);

                                            bulletPosition.x = playerBulletBox.x;
                                            bulletPosition.y = playerBulletBox.y;
                                            Vector2 newVelocity = circleCenters.sub(bulletPosition).nor().scl(800f);


                                            velocity[index].lerp(newVelocity, 0.06f);
                                        }

                                        if (Intersector.overlaps(blackHoleCircle, playerBulletBox)) {
                                            particleTypes[index] = PARTICLE_TYPE.NONE;
                                        }
                                    }
                                }
                            }
                        }

                        //setHitBox(PARTICLE_TYPE.PLAYER_BULLET1, position[index], width[index], height[index]);


                    } else if (particleTypes[index] == PARTICLE_TYPE.LASER_RED) {
                        if (Intersector.intersectSegmentRectangle(position[index], velocity[index], player.getBoundingBox())) {
                            //Gdx.app.log("Laser hit: ", "true");
                            player.takeDamage((int) (bulletDamage[index] * deltaTime));
                        }
                    }
                }

            }
        }
    }

    // Render sprites on screen, some sprites render with rotation
    public void render(SpriteBatch batch) {
        for (int index = 0; index < maxParticles; ++index) {
            if (particleTypes[index] != PARTICLE_TYPE.NONE) {
                if (particleTypes[index] == PARTICLE_TYPE.BULLET1) {
                    batch.draw(bullet1, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.PLAYER_BULLET1) {
                    batch.draw(playerBullet1, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.BULLET2) {
                    batch.draw(bullet2, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.PELLET) {
                    batch.draw(pellet, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.ION) {
                    batch.draw(ionBullet, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.HOLE_BULLET) {
                    batch.draw(holeBullet, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.BLACK_HOLE) {
                    batch.draw(blackHole, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.DOUBLE_BULLET) {
                    batch.draw(doubleBullet, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.HELL_BOSS_BULLET) {
                    batch.draw(pellet, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.BIG_BOSS_BULLET) {
                    batch.draw(bossBullet1, position[index].x, position[index].y, width[index], height[index]);
                } else if (particleTypes[index] == PARTICLE_TYPE.MISSILE) {
                    batch.draw(missile, position[index].x, position[index].y, width[index] / 2, height[index] / 2, width[index], height[index], 1, 1, missileRotation[index]);
                }
                else if (particleTypes[index] == PARTICLE_TYPE.FIRE_ORBIT || particleTypes[index] == PARTICLE_TYPE.FIRE_PROJECTILE)
                {
                    fire = (TextureRegion) fireAnimation.getKeyFrame(stateTime[index], true);

                    batch.draw(fire, position[index].x, position[index].y, width[index]/2, height[index]/2, width[index], height[index], 1, 1, missileRotation[index]);
                }
            }
        }
    }

    public void renderLasers(ShapeRenderer shapeRenderer) {
        for (int index = 0; index < maxParticles; ++index) {
            if (particleTypes[index] != PARTICLE_TYPE.NONE) {
                if (particleTypes[index] == PARTICLE_TYPE.LASER_AIM) {
                    shapeRenderer.setColor(255, 255, 255, 1);
                    shapeRenderer.line(position[index].x, position[index].y, velocity[index].x, velocity[index].y);
                } else if (particleTypes[index] == PARTICLE_TYPE.LASER_RED) {
                    shapeRenderer.setColor(255, 0, 0, 1);
                    shapeRenderer.line(position[index].x, position[index].y, velocity[index].x, velocity[index].y);
                }
            }
        }
    }

    public void dispose() {
        missile.getTexture().dispose();
        bullet1.getTexture().dispose();
        playerBullet1.getTexture().dispose();
        bullet2.getTexture().dispose();
        pellet.getTexture().dispose();
        ionBullet.getTexture().dispose();
        holeBullet.getTexture().dispose();
        blackHole.getTexture().dispose();
        doubleBullet.getTexture().dispose();
        bossBullet1.getTexture().dispose();
        fire.getTexture().dispose();
    }
}

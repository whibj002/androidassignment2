package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class IonEnemy extends Enemy {
    private long lastTime = 0;

    // Adjust difficulty
    private float reloadTime = 500f;
    private float shotSpeed = 400f;
    private float shotLifetime = 2f;
    private float movementSpeed = 300f;
    private float followSpeed = 400f;
    private float chargeSpeed = 500f;
    private float chaseTime = 800f;
    private float chargeTime = 1000f;

    private Vector2 position;

    private Player player;
    private Vector2 target = null;
    private Vector2 rotationVector;
    private Vector2 rotationTarget;
    private Vector2 chargeTarget;
    Vector2 chargeVelocity;
    private Vector2 currentScreenPosition;

    private Vector2 currentShotDirection;


    private float stopScreenHeight = 0.4f;

    public enum ION_STATE {SHOOTING, CHASING, CHARGING}

    private ION_STATE ionState;

    private Polygon boundingPoly;

    private Sound chaseSound;

    public IonEnemy(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.ION;

        setCurrentFrame("enemyShipCustom5.png");
        setHeight(80.0f);
        setWidth(50);

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("EnemyExplosion.png", 1, 8, 0.05f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/ionEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));
        chaseSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/ionEnemyChase.wav")));

        enemyType = EnemyType.ION;

        rotationVector = new Vector2();
        target = new Vector2();
        currentShotDirection = new Vector2();
        chargeTarget = new Vector2();
        currentScreenPosition = new Vector2();
        chargeVelocity = new Vector2();

        health = 120;
        defense = 1;

        position = new Vector2();

        origin.x = width / 2;
        origin.y = height / 2;
        rotation = 0;
        scale.x = 1;
        scale.y = 1;

        ionState = ION_STATE.SHOOTING;
        setCurrentState(EnemyState.MOVING);

        player = particleSystem.getPlayer();
        setUpBoundingPolygon();
    }

    @Override
    public void dispose() {
        super.dispose();
        chaseSound.dispose();
    }


    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 4;
        int yOffset = 3;
        int width = (int) getWidth() - 10;
        int height = (int) getHeight() - 10;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    private void setUpBoundingPolygon() {
        Rectangle r = this.getBoundingBox();

        boundingPoly = new Polygon(new float[]{0, 0,
                r.getWidth(), 0,
                r.getWidth(), r.getHeight(),
                0, r.getHeight()
        });

        boundingPoly.setOrigin(r.getWidth() / 2, r.getHeight() / 2);

        boundingPoly.setScale(1, 1);

    }

    @Override
    public Polygon getBoundingPoly() {
        boundingPoly.setPosition(getScreenPosition().x - getBoundingBox().width / 2, getScreenPosition().y - getBoundingBox().height / 2);


        return boundingPoly;
    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom5_DMG.png");
            setHeight(80.0f);
            setWidth(50);
        }

        if (sound) hurtSound.play();
        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            ionState = ION_STATE.SHOOTING;
            if (sound) dieSound.play();
        }

    }

    public Vector2 getScreenPosition() {
        position.x = super.position.x;
        position.y = super.position.y;

        position.x += width/2;
        position.y += height/2;


        return position;
    }

    public void fire() {
        Vector2 firePosition = new Vector2(getPosition().x - 5, getPosition().y - 20);
        currentShotDirection.x = target.x;
        currentShotDirection.y = target.y;
        Vector2 velocity = (currentShotDirection.sub(firePosition).nor().scl(shotSpeed));


        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.ION, firePosition, 70, 70, velocity, shotLifetime, 0);

        lastTime = System.currentTimeMillis();
        if (sound) fireSound.play();
    }

    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        //player = particleSystem.getPlayer();
        stateTime += deltaTime;
        //origin = getScreenPosition();

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom5.png");
                    setHeight(80.0f);
                    setWidth(50);
                    flashTimer = 0.01f;
                    break;
            }
        }


        rotationTarget = Player.getScreenPosition(playerPosition.x, playerPosition.y);

        switch (ionState) {
            case SHOOTING: {
                switch (getCurrentState()) {
                    case MOVING: {
                        move(0 * deltaTime, -movementSpeed * deltaTime);
                        if (getScreenPosition().y <= screenHeight * 0.82) {
                            isVulnerable = true;
                            setCurrentState(EnemyState.IDLE);
                        }
                    }
                    break;
                    case IDLE: {
                        target.x = playerPosition.x;
                        target.y = playerPosition.y;
                        fire();
                        currentState = EnemyState.RELOAD;
                    }
                    break;
                    case RELOAD: {
                        if (currentTime - lastTime >= reloadTime) {
                            target.x = playerPosition.x;
                            target.y = playerPosition.y;
                            lastTime = currentTime;
                            currentState = EnemyState.FIRE;
                        }
                    }
                    break;
                    case FIRE: {
                        fire();
                        lastTime = currentTime;
                        currentState = EnemyState.RELOAD;
                    }
                    break;

                }

                if (player == null)
                    Gdx.app.log("Player:", "Null");

                if (player.fireState == Player.FireState.DISABLED && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
                    lastTime = currentTime;
                    ionState = ION_STATE.CHASING;
                    if (sound) chaseSound.play();
                }


            }
            break;
            case CHASING: {


                rotationVector.x = rotationTarget.x - getScreenPosition().x;
                rotationVector.y = rotationTarget.y - getScreenPosition().y;


                rotation = rotationVector.angleDeg() + 90;


                boundingPoly.setRotation(rotation);


                Vector2 moveVector = new Vector2(rotationVector);
                moveVector = moveVector.nor().scl(followSpeed);
                move(moveVector.x * deltaTime, moveVector.y * deltaTime);

                if (currentTime - lastTime >= chaseTime) {
                    lastTime = currentTime;
                    chargeTarget.x = rotationTarget.x;
                    chargeTarget.y = rotationTarget.y;

                    //chargeTarget = new Vector2(rotationTarget);
                    currentScreenPosition = new Vector2(getScreenPosition());

                    chargeVelocity = chargeTarget.sub(currentScreenPosition).nor().scl(chargeSpeed);


                    ionState = ION_STATE.CHARGING;

                }
            }
            break;
            case CHARGING: {


                move(chargeVelocity.x * deltaTime, chargeVelocity.y * deltaTime);

                if (currentTime - lastTime >= chargeTime) {
                    Gdx.app.log("Exploding", "True");
                    currentState = EnemyState.EXPLODING;
                    ionState = ION_STATE.SHOOTING;
                }
            }
            break;
        }

        switch (currentState) {
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;
            }
            case DEAD: {
            }
            break;
        }
    }


    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));
        super.render2(batch);
    }
}

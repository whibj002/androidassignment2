package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.mygdx.game.Items.Item;
import com.mygdx.game.SaveFile;

import java.util.Random;

//Work in progress
public abstract class Enemy extends Sprite {

    public enum EnemyState {IDLE, RELOAD, FIRE, MOVING, DEAD, EXPLODING, AIM}

    public enum EnemyType {NONE, BASIC, ELITE_BASIC, LASER, ELITE_LASER, SPLIT_SHOT, ELITE_SPLIT_SHOT, ION, HOLE, DOUBLE_SHOT, BULLET_HELL_BOSS, BIG_BOSS, GOON}

    protected EnemyState currentState;
    protected EnemyType enemyType;

    protected int health;
    protected int defense;
    protected int damageOutput;
    protected Item loot;
    protected boolean isVulnerable;

    protected int screenWidth;
    protected int screenHeight;

    protected ParticleSystem particleSystem;

    protected boolean looping = false;
    protected float stateTime;
    protected Animation currentAnimation;
    protected Animation deathAnimation;

    protected Sound fireSound;
    protected Sound dieSound;
    protected Sound hurtSound;

    protected SaveFile saveFile;
    protected boolean sound;

    public enum FlashState{FLASHING, NOT_FLASHING}
    public FlashState flashState;
    public float flashTimer = 0.01f;


    public Enemy(ParticleSystem p) {
        super();
        this.particleSystem = p;
        saveFile = new SaveFile();
        sound = saveFile.getSound();
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        flashState = FlashState.NOT_FLASHING;
    }

    @Override
    public void dispose() {
        super.dispose();
        if (fireSound != null) {
            fireSound.dispose();
        }
        if (dieSound != null) {
            dieSound.dispose();
        }
        if (hurtSound != null) {
            hurtSound.dispose();
        }
    }

    /**
     * Decreases enemy health
     * @param amount
     */
    public void takeDamage(int amount) {
        if (isVulnerable) {
            amount /= defense;
            if (health - amount <= 0 && currentState != EnemyState.DEAD && currentState != EnemyState.EXPLODING) {
                health = 0;
            } else {
                health -= amount;
            }
        }
    }

    /**
     * @return Rectangle offset to match sprite */
    public Rectangle getBoundingBox() {
        return new Rectangle();
    }

    /**
     * Update the enemy movement
     * @param deltaTime
     * @param playerPosition
     * @param currentTime
     */
    public abstract void update(float deltaTime, Vector2 playerPosition, long currentTime);

    public void addItem(Item item) {
        loot = item;
    }

    public Item getItem() {
        return loot;
    }

    public boolean isDead() {
        return currentState == EnemyState.DEAD;
    }

    public EnemyState getCurrentState() {
        return currentState;
    }

    public EnemyType getEnemyType() {
        return enemyType;
    }

    public void setCurrentState(EnemyState state) {
        currentState = state;
    }

    public Polygon getBoundingPoly() {
        return new Polygon();
    }

    protected int random(int start, int finish) {
        Random rand = new Random();
        return rand.nextInt(finish + 1 - start) + start;
    }


}

package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.SaveFile;

public class DoubleShot extends Enemy {
    private enum MOVE_STATE {NONE, CHOOSE_DEST, MOVE}

    private float movementSpeed = 300f;
    private float reloadTime = 700f;

    private long lastTime = 0;
    long moveLastTime = 0;

    private float shotSpeed = 300f;
    private float shotLifeTime = 2f;
    private float dodgeSpeed = 300f;
    private float moveTime = 700f;

    private Vector2 target;
    private Vector2 shotDirection;
    private Vector2 moveVelocity;
    Vector2 firePosition;

    private MOVE_STATE moveState;

    public DoubleShot(ParticleSystem p) {
        super(p);
        enemyType = EnemyType.DOUBLE_SHOT;

        setCurrentFrame("enemyShipCustom4.png");
        setHeight(128.0f);
        setWidth(128.0f);

        fireSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/doubleEnemyFire.wav")));
        dieSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/explosion.wav")));
        hurtSound = Gdx.audio.newSound((Gdx.files.internal("gameplaySfx/enemyHit.wav")));

        currentAnimation = null;
        deathAnimation = Sprite.loadAnimation("playerDeath.png", 1, 12, 0.05f);

        firePosition = new Vector2();
        target = new Vector2();
        shotDirection = new Vector2(0, -1);

        damageOutput = 5;
        health = 200;
        defense = 1;
        stateTime = 0;

        moveVelocity = new Vector2();
        moveState = MOVE_STATE.NONE;
        setCurrentState(EnemyState.MOVING);
    }

    @Override
    public Rectangle getBoundingBox() {
        int xOffset = 30;
        int yOffset = 30;
        int width = (int) getWidth() - 60;
        int height = (int) getHeight() - 50;
        return super.getBoundingBox(xOffset, yOffset, width, height);
    }

    public Vector2 getScreenPosition() {
        Vector2 position = new Vector2(super.position.x, super.position.y);

        position.x += 240;
        position.y += 290;


        return position;
    }

    public void fire() {

        firePosition.x = getPosition().x + getWidth() / 2 - 65;
        firePosition.y = getPosition().y - 60;

        Vector2 velocity = (shotDirection.scl(shotSpeed));

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.DOUBLE_BULLET, firePosition, 75, 200, velocity, shotLifeTime, damageOutput);

        shotDirection.x = 0;
        shotDirection.y = -1;

        firePosition.x += 55;

        velocity = (shotDirection.scl(shotSpeed));

        particleSystem.spawn(ParticleSystem.PARTICLE_TYPE.DOUBLE_BULLET, firePosition, 75, 200, velocity, shotLifeTime, damageOutput);

        shotDirection.x = 0;
        shotDirection.y = -1;
        if (sound) fireSound.play();

    }

    @Override
    public void takeDamage(int amount) {
        super.takeDamage(amount);

        if (currentState != EnemyState.EXPLODING) {
            flashState = FlashState.FLASHING;
            getCurrentFrame().getTexture().dispose();
            setCurrentFrame("dmgShips/enemyShipCustom4_DMG.png");
            setHeight(128.0f);
            setWidth(128.0f);
        }

        if (sound) hurtSound.play();

        if (health <= 0 && currentState != EnemyState.EXPLODING && currentState != EnemyState.DEAD) {
            lastTime = System.currentTimeMillis();
            stateTime = 0;
            moveState = MOVE_STATE.NONE;
            setCurrentFrame((TextureRegion) deathAnimation.getKeyFrames()[0]);
            currentState = EnemyState.EXPLODING;
            if (sound) dieSound.play();
        }
    }


    @Override
    public void update(float deltaTime, Vector2 playerPosition, long currentTime) {
        //Gdx.app.log("Current y coord", Float.toString(getPosition().y));
        stateTime += deltaTime;

        if (health > 0) {
            switch (flashState) {
                case FLASHING:
                    flashTimer-= deltaTime;
                    if (flashTimer <= 0) {
                        flashState = FlashState.NOT_FLASHING;
                    }
                    break;
                case NOT_FLASHING:
                    getCurrentFrame().getTexture().dispose();
                    setCurrentFrame("enemyShipCustom4.png");
                    setHeight(128.0f);
                    setWidth(128.0f);
                    flashTimer = 0.01f;
                    break;
            }
        }

        switch (currentState) {
            case MOVING: {
                move(-movementSpeed * deltaTime, 0);
                if (position.x < screenWidth /2f - width/2f) {
                    isVulnerable = true;
                    setCurrentState(EnemyState.IDLE);
                    moveState = MOVE_STATE.CHOOSE_DEST;
                }
            }
            break;
            case IDLE: {
                lastTime = currentTime;
                setCurrentState(EnemyState.RELOAD);
            }
            break;
            case RELOAD: {
                if (currentTime - lastTime >= reloadTime) {

                    target = new Vector2(playerPosition);


                    setCurrentState(EnemyState.FIRE);
                }
            }
            break;
            case FIRE: {
                fire();
                lastTime = currentTime;
                currentState = EnemyState.RELOAD;

            }
            break;
            case EXPLODING: {
                currentAnimation = deathAnimation;

                if (currentTime - lastTime >= 400)
                    currentState = EnemyState.DEAD;

            }
            break;
            default:
                Gdx.app.log("State:", "Unhandled state");
        }


        switch (moveState) {
            case CHOOSE_DEST: {
                int randomX = random((int) width, screenWidth * 2);
                int randomY = random((int) (screenHeight * 0.90), (int) (screenHeight * 0.90));

                Vector2 destination = new Vector2(randomX, randomY);

                moveVelocity = getScreenPosition().sub(destination).nor().scl(-dodgeSpeed);
                moveLastTime = currentTime;
                moveState = DoubleShot.MOVE_STATE.MOVE;
            }
            break;
            case MOVE: {
                if (currentState != EnemyState.MOVING) {
                    move(moveVelocity.x * deltaTime, moveVelocity.y * deltaTime);

                    if (position.x <= 0 || position.x + width > screenWidth || position.y < 0 || position.y + height > screenHeight) {
                        moveState = DoubleShot.MOVE_STATE.CHOOSE_DEST;
                    }

                    if (currentTime - moveLastTime >= moveTime)
                        moveState = DoubleShot.MOVE_STATE.CHOOSE_DEST;
                }
            }
            break;
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (currentAnimation != null)
            setCurrentFrame((TextureRegion) currentAnimation.getKeyFrame(stateTime, looping));

        super.render(batch);
    }

}

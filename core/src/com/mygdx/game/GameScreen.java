package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Entities.Enemy;
import com.mygdx.game.Items.Item;
import com.mygdx.game.Level.BasicWave;
import com.mygdx.game.Level.BossWave;
import com.mygdx.game.Level.EliteWave;
import com.mygdx.game.Level.TypeWave;
import com.mygdx.game.Level.Wave;
import com.mygdx.game.Level.Level;
import com.mygdx.game.Entities.ParticleSystem;
import com.mygdx.game.Entities.Player;
import com.mygdx.game.Background.ScrollingBackground;

public class GameScreen implements Screen {
    // Instance variables
    MyGdxGame game;

    // Game state
    public enum GameState {PLAYING, FAILED, LEVEL_COMPLETE, GAME_COMPLETE, NEXT_LEVEL}

    private GameState gameState;

    private SpriteBatch batch;
    private ShapeRenderer hitBoxes;
    private Stage stage;

    private Player player;

    //private BulletHellBoss testEnemy;
    private Level[] levels;
    private Level level;
    private int currentLevelIndex;
    private Wave[] waves;
    private Wave currentWave;
    private Item currentItem;

    private ParticleSystem particleSystem;

    private float deltaTime;
    private float stateTime;
    private long currentTime;

    private int damageMultiplier;
    private int defenseMultiplier;

    //Icons
    private Texture damageBoostIcon;
    private Texture defenseBoostIcon;
    private Texture virtualCurrencyIcon;

    //Background
    private ScrollingBackground background;
    //Health bar
    private float healthBar;
    private Texture blank;
    private final float waitTime = 3f;
    private float timeSinceDeath;

    // Fail states
    private Skin skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
    private TextButton restart = new TextButton("Restart", skin);
    private TextButton exitToMenu = new TextButton("Menu", skin);
    private TextButton nextLevel = new TextButton("Next Level", skin);

    private float w;
    private float h;

    private SaveFile saveFile;

    BitmapFont vcAmount;

    // Constructor
    public GameScreen(MyGdxGame game) {
        this.game = game;
        create();
    }

    public void create() {
        saveFile = new SaveFile();

        particleSystem = new ParticleSystem();
        particleSystem.setUp();
        player = new Player(particleSystem);
        particleSystem.setPlayer(player);

        batch = new SpriteBatch();
        hitBoxes = new ShapeRenderer();
        stage = new Stage();

        //background
        background = new ScrollingBackground();

        //Icons
        damageBoostIcon = new Texture("items/damageBoost.png");
        defenseBoostIcon = new Texture("items/defenseBoost.png");
        virtualCurrencyIcon = new Texture("items/vc.png");
        vcAmount = new BitmapFont();

        //Health bar
        blank = new Texture("blank.png");

        //Levels
        levels = buildLevels();
        currentLevelIndex = saveFile.getCurrentLevel();
        level = levels[currentLevelIndex];

        // Fail state
        // Restart
        restart.setSize((float) Gdx.graphics.getWidth() / 2, (float) Gdx.graphics.getHeight() / 10);
        restart.getLabel().setFontScale(3, 3);
        restart.setPosition((float) Gdx.graphics.getWidth() / 4f, (float) Gdx.graphics.getHeight() / 2f);
        // Next Level
        nextLevel.setSize((float) Gdx.graphics.getWidth() / 2, (float) Gdx.graphics.getHeight() / 10);
        nextLevel.getLabel().setFontScale(3, 3);
        nextLevel.setPosition((float) Gdx.graphics.getWidth() / 4f, (float) Gdx.graphics.getHeight() / 2f);
        // Exit to menu
        exitToMenu.setSize((float) Gdx.graphics.getWidth() / 2, (float) Gdx.graphics.getHeight() / 10);
        exitToMenu.getLabel().setFontScale(3, 3);
        exitToMenu.setPosition((float) Gdx.graphics.getWidth() / 4f, ((float) Gdx.graphics.getHeight() / 2f) - (float) Gdx.graphics.getHeight() / 7);

        w = Gdx.graphics.getWidth();
        h = Gdx.graphics.getHeight();

        set();
    }

    public void set() {
        player.updateFromSaveFile();
        player.setPosition(w / 2 - player.getWidth() / 2, h / 5);
        player.setCurrentState(Player.PlayerState.MOVING);

        currentLevelIndex = saveFile.getCurrentLevel();
        level = levels[currentLevelIndex];
        level.initialise();

        Gdx.input.setInputProcessor(null);

        // Gamestate testing
        gameState = GameState.PLAYING;
        timeSinceDeath = 0;
    }

    @Override
    public void show() {

    }

    public void update() {
        deltaTime = Gdx.graphics.getDeltaTime();
        currentTime = System.currentTimeMillis();
        background.update(deltaTime);
        healthBar = (float) player.getHealth() / player.getMaxHealth();
        damageMultiplier = player.getDamageMultiplier();
        defenseMultiplier = player.getDefenseMultiplier();

        if (level != null) {
            currentWave = level.getCurrentWave();
            if (currentWave != null) {
                currentItem = currentWave.getItem();
            }
        }

        switch (gameState) {
            case NEXT_LEVEL:
                if (currentLevelIndex < saveFile.getMaxLevels()) {
                    saveFile.increaseCurrentLevel();
                    set();
                } else {
                    gameState = GameState.GAME_COMPLETE;
                    dispose();
                    game.setScreen(new MenuScreen(game));
                }
                break;
            case PLAYING:
                particleSystem.update(deltaTime, player.getPosition().x, player.getPosition().y);

                level.update(deltaTime, player.getPosition(), currentTime);

                int moveX = 0;
                int moveY = 0;
                // Move the player
                if (Gdx.input.isTouched()) {
                    moveX = Gdx.input.getDeltaX();
                    moveY = -Gdx.input.getDeltaY();

                    if (player.getBoundingBox().getX() <= 0) {
                        moveX = 0;
                        player.setPosition(player.getPosition().x + (0 - player.getBoundingBox().getX()) + 1, player.getPosition().y);
                    }
                    if (player.getBoundingBox().getX() + player.getBoundingBox().getWidth() >= Gdx.graphics.getWidth()) {
                        moveX = 0;
                        player.setPosition(player.getPosition().x - (player.getBoundingBox().getX() + player.getBoundingBox().getWidth() - Gdx.graphics.getWidth()) - 1, player.getPosition().y);
                    }
                    if (player.getBoundingBox().getY() <= 0) {
                        moveY = 0;
                        player.setPosition(player.getPosition().x, player.getPosition().y + (0 - player.getBoundingBox().getY()) + 1);
                    }
                    if (player.getBoundingBox().getY() + player.getBoundingBox().getHeight() >= Gdx.graphics.getHeight()) {
                        moveY = 0;
                        player.setPosition(player.getPosition().x, player.getPosition().y - (player.getBoundingBox().getY() + player.getBoundingBox().getHeight() - Gdx.graphics.getHeight()) - 1);
                    }
                }
                player.update(deltaTime, currentTime, moveX, moveY);

                //Item Collision
                if (currentItem != null) {
                    if (!currentItem.isCollected()) {
                        currentItem.update();
                        if (player.getBoundingBox().overlaps(currentItem.getBoundingBox())) {
                            player.collectItem(currentItem);
                        }
                    }
                }

                //Player/Enemy Collision
                if (currentWave != null) {
                    for (int i = 0; i < currentWave.getNumOfEnemies(); i++) {
                        if (player != null && currentWave.getEnemies()[i] != null) {
                            if (currentWave.getEnemies()[i].getEnemyType() == Enemy.EnemyType.ION) {
                                if (Intersector.overlapConvexPolygons(player.getBoundingPoly(), currentWave.getEnemies()[i].getBoundingPoly())) {
                                    player.takeDamage(player.getMaxHealth());
                                    currentWave.getEnemies()[i].takeDamage(1000);
                                }
                            }

                            if (currentWave.getEnemies()[i].getEnemyType() == Enemy.EnemyType.BIG_BOSS) {
                                if (player.getBoundingBox().overlaps(currentWave.getEnemies()[i].getBoundingBox())) {
                                    player.takeDamage(80);
                                    continue;
                                }
                            }

                            if (player.getBoundingBox().overlaps(currentWave.getEnemies()[i].getBoundingBox())) {
                                player.takeDamage(player.getMaxHealth() / 8);
                            }
                            if (currentWave.getNumOfEnemies() <= 0) {
                                player.fireState = Player.FireState.ACTIVE;
                            }
                        }
                    }
                }
                // Check if player is dead
                if (player.isDead()) {
                    timeSinceDeath += deltaTime;
                    player.setCurrentState(Player.PlayerState.DEAD);
                    if (timeSinceDeath >= waitTime) {
                        gameState = GameState.FAILED;
                    }
                }
                // If all waves are done, the level is finished
                if (level.isFinished()) {
                    if (saveFile.getHighestCompletedLevel() <= currentLevelIndex) {
                        saveFile.setHighestCompletedLevel(currentLevelIndex + 1);
                    }
                    gameState = GameState.LEVEL_COMPLETE;
                }
                break;
            case FAILED:
                break;
        }
    }


    @Override
    public void render(float delta) {
        update();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();

        //Background
        background.render(batch);

        //Player & Enemy
        particleSystem.render(batch);
        player.render(batch);
        //testEnemy.render(batch);
        level.render(batch);

        //Item
        if (level.getCurrentWave() != null) {
            if (level.getCurrentWave().getItem() != null && !level.getCurrentWave().getItem().isCollected() && !level.isLastWave()) {
                level.getCurrentWave().getItem().render(batch);
            }
        }

        //Player Health Bar
        if (healthBar > 0.75) {
            batch.setColor(Color.GREEN);
        } else if (healthBar > 0.50 && healthBar <= 0.75) {
            batch.setColor(Color.YELLOW);
        } else if (healthBar > 0.25 && healthBar <= 0.50) {
            batch.setColor(Color.ORANGE);
        } else if (healthBar <= 0.25) {
            batch.setColor(Color.RED);
        }
        batch.draw(blank, 0, 0, Gdx.graphics.getWidth() * healthBar, 5);
        batch.setColor(Color.WHITE);

        //Boosts/Virtual Currency
        for (int i = 0; i < Math.max(damageMultiplier, defenseMultiplier); i++) {
            if (i < damageMultiplier - 1) {
                batch.draw(damageBoostIcon, (i * damageBoostIcon.getWidth() / 5f), virtualCurrencyIcon.getHeight(), damageBoostIcon.getWidth() / 5f, damageBoostIcon.getHeight() / 5f);
            }
            if (i < defenseMultiplier - 1) {
                batch.draw(defenseBoostIcon, (
                        i * defenseBoostIcon.getWidth() / 5f), virtualCurrencyIcon.getHeight() + damageBoostIcon.getHeight() / 5f, defenseBoostIcon.getWidth() / 5f, defenseBoostIcon.getHeight() / 5f);
            }
        }
        batch.draw(virtualCurrencyIcon, 0, 0, virtualCurrencyIcon.getWidth(), virtualCurrencyIcon.getHeight());
        vcAmount.setColor(Color.YELLOW);
        vcAmount.draw(batch, Integer.toString(player.getVirtualCurrency()), virtualCurrencyIcon.getWidth(), virtualCurrencyIcon.getHeight() / 1.5f);
        batch.end();

        // Fail state checking
        if (gameState == GameState.FAILED) {
            stage.addActor(restart);
            stage.addActor(exitToMenu);
            initClickListeners();
            Gdx.input.setInputProcessor(stage);
            stage.draw();
        }
        // Level Completed
        if (gameState == GameState.LEVEL_COMPLETE) {
            stage.addActor(nextLevel);
            stage.addActor(exitToMenu);
            initClickListeners();
            Gdx.input.setInputProcessor(stage);
            stage.draw();
        }

        //Player/Enemy/Bullets
        hitBoxes.begin(ShapeRenderer.ShapeType.Line);
        hitBoxes.setColor(0, 0, 255, 1);
        particleSystem.renderLasers(hitBoxes);
        hitBoxes.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        background.dispose();
        blank.dispose();
        damageBoostIcon.dispose();
        defenseBoostIcon.dispose();
        virtualCurrencyIcon.dispose();
        player.dispose();
        stage.dispose();
        batch.dispose();
        particleSystem.dispose();
    }

    private void initClickListeners() {
        restart.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                create();
            }
        });
        exitToMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                game.setScreen(new MenuScreen(game));
            }
        });

        nextLevel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameState = GameState.NEXT_LEVEL;
            }
        });
    }

    private Level[] buildLevels() {
        return new Level[]{buildLevel1(), buildLevel2(), buildLevel3(), buildLevel4(), buildLevel5(), buildLevel6(), buildLevel7(), buildLevel8(), buildLevel9(), buildLevel10()};
    }

    private Level buildLevel1() {
        Wave[] waves = new Wave[]{
                new BasicWave(1, false), new BasicWave(1, true), new BasicWave(1, false),
                new BasicWave(2, true), new BasicWave(2, false), new BasicWave(2, true),
                new BasicWave(2, false), new BasicWave(3, true), new EliteWave(3, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel2() {
        Wave[] waves = new Wave[]{
                new BasicWave(1, false), new BasicWave(1, false), new BasicWave(2, true),
                new BasicWave(2, false), new BasicWave(2, true), new EliteWave(2, false),
                new BasicWave(3, true), new BasicWave(3, true), new BasicWave(4, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel3() {
        Wave[] waves = new Wave[]{
                new BasicWave(1, false), new BasicWave(2, true), new BasicWave(2, false),
                new EliteWave(2, true), new BasicWave(3, false), new BasicWave(3, true),
                new EliteWave(3, true), new BasicWave(4, true), new EliteWave(4, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel4() {
        Wave[] waves = new Wave[]{
                new BasicWave(2, false), new TypeWave(2, Enemy.EnemyType.BASIC, false), new EliteWave(3, true),
                new TypeWave(3, Enemy.EnemyType.ELITE_BASIC, false), new BasicWave(4, true), new BasicWave(4, false),
                new BasicWave(4, true), new EliteWave(4, true), new EliteWave(5, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel5() {
        Wave[] waves = new Wave[]{
                new BasicWave(3, false), new BasicWave(3, true), new EliteWave(3, false),
                new BasicWave(4, true), new EliteWave(4, false), new TypeWave(4, Enemy.EnemyType.BASIC, true),
                new BasicWave(5, false), new EliteWave(5, true), new TypeWave(5, Enemy.EnemyType.LASER, true),
                new BossWave(Enemy.EnemyType.BULLET_HELL_BOSS)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel6() {
        Wave[] waves = new Wave[]{
                new BasicWave(3, false), new EliteWave(3, true), new BasicWave(4, false),
                new EliteWave(4, true), new BasicWave(5, false), new EliteWave(5, false),
                new TypeWave(4, Enemy.EnemyType.ELITE_BASIC, true), new BasicWave(5, false), new EliteWave(5, true),
                new TypeWave(3, Enemy.EnemyType.ION, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel7() {
        Wave[] waves = new Wave[]{
                new BasicWave(4, false), new EliteWave(4, true), new BasicWave(5, false),
                new EliteWave(5, true), new EliteWave(5, false), new TypeWave(4, Enemy.EnemyType.BASIC, true),
                new BasicWave(4, false), new TypeWave(4, Enemy.EnemyType.ELITE_BASIC, true), new EliteWave(5, true),
                new TypeWave(4, Enemy.EnemyType.ELITE_LASER, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }


    private Level buildLevel8() {
        Wave[] waves = new Wave[]{
                new BasicWave(4, false), new EliteWave(4, true), new BasicWave(5, false),
                new EliteWave(5, true), new EliteWave(5, false), new TypeWave(5, Enemy.EnemyType.ELITE_BASIC, true),
                new EliteWave(5, false), new EliteWave(5, true), new TypeWave(4, Enemy.EnemyType.ELITE_LASER, true),
                new TypeWave(4, Enemy.EnemyType.ION, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel9() {
        Wave[] waves = new Wave[]{
                new EliteWave(4, false), new EliteWave(4, true), new TypeWave(5, Enemy.EnemyType.ELITE_BASIC, false),
                new EliteWave(5, true), new EliteWave(5, false), new TypeWave(5, Enemy.EnemyType.ELITE_LASER, true),
                new EliteWave(5, false), new EliteWave(5, true), new TypeWave(4, Enemy.EnemyType.ION, true),
                new TypeWave(5, Enemy.EnemyType.ELITE_LASER, true)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }

    private Level buildLevel10() {
        Wave[] waves = new Wave[]{
                new TypeWave(5, Enemy.EnemyType.BASIC, false), new EliteWave(5, true),
                new TypeWave(5, Enemy.EnemyType.LASER, false), new EliteWave(5, true),
                new TypeWave(5, Enemy.EnemyType.ELITE_BASIC, false), new EliteWave(5, true),
                new TypeWave(5, Enemy.EnemyType.ELITE_LASER, false), new EliteWave(5, true),
                new TypeWave(4, Enemy.EnemyType.ION, true), new EliteWave(5, true),
                new TypeWave(5, Enemy.EnemyType.ION, true), new EliteWave(5, true),
                new BossWave(Enemy.EnemyType.BIG_BOSS)
        };
        Level level = new Level(particleSystem);
        level.setWaves(waves);
        return level;
    }
}